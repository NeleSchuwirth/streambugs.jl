using Streambugs
using Test
using Printf
using BenchmarkTools

# import DiffEqBase
import ForwardDiff


if endswith(pwd(), "test")
    cd("..")
end
println("working dir: ", pwd())

include("testutils.jl")

include("unittests.jl")
include("systemtests.jl")
include("benchmarks.jl")

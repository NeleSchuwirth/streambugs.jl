using Printf


@testset "interpolate" begin
    x = [2.3, 6.7, 9.1]
    y = [-4.5, 3.1, 1.1]
    @test Streambugs.interpolate(x, y, 2.1) ≈ -4.5
    @test Streambugs.interpolate(x, y, 2.3) ≈ -4.5
    @test Streambugs.interpolate(x, y, 2.5) ≈ -4.15454545
    @test Streambugs.interpolate(x, y, 6.7) ≈ 3.1
    @test Streambugs.interpolate(x, y, 7.9) ≈ 2.1
    @test Streambugs.interpolate(x, y, 9.1) ≈ 1.1
    @test Streambugs.interpolate(x, y, 10.0) ≈ 1.1
    @test Streambugs.interpolate(reshape(cat(x, y, dims = 1), :, 2), 6.7) ≈ 3.1
    @test Streambugs.interpolate(reshape(cat(x, y, dims = 1), :, 2), 10.0) ≈ 1.1
    @test Streambugs.interpolate(1.1, 2.5) ≈ 1.1
    @test Streambugs.interpolate(1.1, 5.0) ≈ 1.1
    @test Streambugs.interpolate(11.0, 5.0) ≈ 11
    @test Streambugs.interpolate(Streambugs.Input("x", 11.0), 5.0) ≈ 11
    @test Streambugs.interpolate(
        Streambugs.Input("x", reshape(cat(x, y, dims = 1), :, 2)),
        6.7,
    ) ≈ 3.1
end

@testset "paste" begin
    @test Streambugs.paste("alpha", 1:2, ["a", "b", "c"]) ==
          ["alpha 1 a", "alpha 2 b", "alpha 1 c"]
    @test Streambugs.paste("alpha", 1:2, ["a", "b", "c"], sep = "_") ==
          ["alpha_1_a", "alpha_2_b", "alpha_1_c"]
    @test Streambugs.paste("alpha", 1:2, ["a", "b", "c"], sep = "") ==
          ["alpha1a", "alpha2b", "alpha1c"]
    @test Streambugs.paste("alpha", 1:2, ["a", "b", "c"], sep = "", collapse = "|") ==
          "alpha1a|alpha2b|alpha1c"
    @test Streambugs.paste("alpha", 1:2, ["a", "b", "c"], sep = "", collapse = "") ==
          "alpha1aalpha2balpha1c"
    @test Streambugs.paste(2.5, "a", 1:3, 8:2:10, collapse = "|") ==
          "2.5 a 1 8|2.5 a 2 10|2.5 a 3 8"
    @test Streambugs.paste("alpha") == ["alpha"]
    @test Streambugs.paste("alpha", collapse = "|") == "alpha"
    @test Streambugs.paste(42) == ["42"]
    @test Streambugs.paste(42, 1:2) == ["42 1", "42 2"]
    @test Streambugs.paste() == []
end

@testset "matchfirst" begin
    @test Streambugs.matchfirst(["a", "b"], ["b", 12, "c", "a", "b", "a"]) == [4, 1]
    @test Streambugs.matchfirst(["a", "d"], ["b", 12, "c", "a", "b", "a"]) == [4, 0]
    @test Streambugs.matchfirst([], ["b", 12, "c", "a", "b", "a"]) == []
end

@testset "factor" begin
    @test Streambugs.factor(["a", "c", "a", "b"]) == (["a", "c", "b"], [1, 2, 1, 3])
    @test Streambugs.factor(["a", 42, "a", "b"]) == (["a", 42, "b"], [1, 2, 1, 3])
    @test Streambugs.factor(["a"]) == (["a"], [1])
    @test Streambugs.factor([]) == ([], [])
end

@testset "construct_statevariables" begin
    @test Streambugs.construct_statevariables(["a", "b"], ["h"], pom = 1:2) ==
          ["a_h_1_POM", "a_h_2_POM", "b_h_1_POM", "b_h_2_POM"]
    @test Streambugs.construct_statevariables(
        ["a"],
        ["h"],
        algae = 1:2,
        invertebrates = ["i"],
    ) == ["a_h_1_Algae", "a_h_2_Algae", "a_h_i_Invertebrates"]
    @test Streambugs.construct_statevariables(
        ["a", "a"],
        ["h", "h"],
        algae = [1, 1],
        invertebrates = ["i"],
    ) == ["a_h_1_Algae", "a_h_i_Invertebrates"]
end

@testset "get_y_names_indices_by_name_hierarchy" begin
    y_names = ["R1_H1_T1_T", "R1_H1_T2_T", "R1_H2_T2", "R2_H1_T1"]
    @assert_equals(
        Streambugs.get_y_names_indices_by_name_hierarchy(y_names),
        Dict(
            "R1" => Dict("H1" => Dict("T1" => 1, "T2" => 2), "H2" => Dict("T2" => 3)),
            "R2" => Dict("H1" => Dict("T1" => 4)),
        )
    )
    @test_throws(
        ArgumentError("State variable 'R_T' has less than 3 parts."),
        Streambugs.get_y_names_indices_by_name_hierarchy(["R_T"])
    )
    @test_throws(
        ArgumentError(
            "State variable 'R_H_T' is invalid because there is " *
            "already a state variable with same reach, habitat and taxon.",
        ),
        Streambugs.get_y_names_indices_by_name_hierarchy(["R_H_T_G", "R_H_T"])
    )
end

@testset "streambugs_example_model_toy" begin
    model = Streambugs.streambugs_example_model_toy()
    expected_toy_model = read(open("test/expected_toy_model.txt"), String)
    actual_toy_model = render_pretty(model)
    @assert_equals actual_toy_model expected_toy_model
end

@testset "get_values_and_indices_global" begin
    input = [
        Streambugs.Input("x", 99.0),
        Streambugs.Input("y", 3.4),
        Streambugs.Input("b", 42.0),
        Streambugs.Input("c", 23.0),
    ]
    parameters = Dict("a" => 2.3, "b" => 8.3)
    defaults = Dict("a" => 1.11, "c" => 2.9, "d" => 22.6)
    @test Streambugs.get_values_and_indices_global(
        ["a", "b"],
        parameters,
        input,
        defaults,
    ) == Dict(
        "a" => Streambugs.ValueAndInputIndex(2.3, 0),
        "b" => Streambugs.ValueAndInputIndex(8.3, 3),
    )
    @test isequal(
        Streambugs.get_values_and_indices_global(["y", "c"], parameters, input, defaults),
        Dict(
            "y" => Streambugs.ValueAndInputIndex(NaN, 2),
            "c" => Streambugs.ValueAndInputIndex(2.9, 4),
        ),
    )
    @test isequal(
        Streambugs.get_values_and_indices_global(["z", "c"], parameters, input),
        Dict(
            "z" => Streambugs.ValueAndInputIndex(NaN, 0),
            "c" => Streambugs.ValueAndInputIndex(NaN, 4),
        ),
    )
    @test isequal(
        Streambugs.get_values_and_indices_global(
            ["y", "c"],
            parameters,
            Streambugs.Input{Float64}[],
            defaults,
        ),
        Dict(
            "y" => Streambugs.ValueAndInputIndex(NaN, 0),
            "c" => Streambugs.ValueAndInputIndex(2.9, 0),
        ),
    )
    @test isequal(
        Streambugs.get_values_and_indices_global(["y", "c"], parameters),
        Dict(
            "y" => Streambugs.ValueAndInputIndex(NaN, 0),
            "c" => Streambugs.ValueAndInputIndex(NaN, 0),
        ),
    )
end

@testset "get_values_and_indices_global_envcondtraits" begin
    model = Streambugs.streambugs_example_model_toy()
    y_names = model.y_names
    pars = model.parameters
    inps = model.inputs
    push!(inps, Streambugs.Input("Algae_M", 9.2))

    @assert_equals(
        Streambugs.get_values_and_indices_global_envcondtraits(["Algae"], pars, inps),
        Dict(
            "Algae" => Dict(
                "Dini" => Streambugs.ValueAndInputIndex(100.0, 0),
                "EC" => Streambugs.ValueAndInputIndex(20000.0, 0),
                "Ea" => Streambugs.ValueAndInputIndex(0.231, 0),
                "KI" => Streambugs.ValueAndInputIndex(62.1, 0),
                "KN" => Streambugs.ValueAndInputIndex(0.1, 0),
                "KP" => Streambugs.ValueAndInputIndex(0.01, 0),
                "M" => Streambugs.ValueAndInputIndex(1e-7, 2),
                "b" => Streambugs.ValueAndInputIndex(0.93, 0),
                "cdet" => Streambugs.ValueAndInputIndex(2.0, 0),
                "fdeath" => Streambugs.ValueAndInputIndex(0.7, 0),
                "fprod" => Streambugs.ValueAndInputIndex(5.0, 0),
                "fresp" => Streambugs.ValueAndInputIndex(1.0, 0),
                "hdens" => Streambugs.ValueAndInputIndex(1000.0, 0),
                "i0" => Streambugs.ValueAndInputIndex(4150455552.0, 0),
            ),
        )
    )
    @assert_equals(
        Streambugs.get_values_and_indices_global_envcondtraits(["unknown"], pars, inps),
        Dict("unknown" => Dict{String,Streambugs.ValueAndInputIndex{Float64}}())
    )
end

@testset "get_values_and_indices" begin
    par = Dict("b" => 2.3, "a1_b" => 4.5)
    inp = [Streambugs.Input("b", 43.5)]
    @assert_equals(
        Streambugs.get_values_and_indices(
            [["a1_", ""], ["a2_", ""]],
            [1, 2],
            ["b"],
            par,
            inp,
        ),
        Dict(
            "b" => Streambugs.convert_to_values_and_input_indices([
                Streambugs.ValueAndInputIndex(4.5, 1),
                Streambugs.ValueAndInputIndex(2.3, 1),
            ]),
        )
    )
    par = Dict("x" => 2.3, "a1_b" => 4.5)
    inp = [Streambugs.Input("a2_b", 43.5)]
    @assert_equals(
        Streambugs.get_values_and_indices(
            [["a1_", ""], ["a2_", ""]],
            [1, 2],
            ["b"],
            par,
            inp,
        ),
        Dict(
            "b" => Streambugs.convert_to_values_and_input_indices([
                Streambugs.ValueAndInputIndex(4.5, 0),
                Streambugs.ValueAndInputIndex(NaN, 1),
            ]),
        )
    )
end

@testset "get_values_and_indices_reach" begin
    model = Streambugs.streambugs_example_model_toy()
    y_names = model.y_names
    pars = model.parameters
    inps = model.inputs
    defaults = Dict("fA" => 3.5)
    @assert_equals(
        Streambugs.get_values_and_indices_reach(["w", "L"], y_names, pars, inps),
        Dict(
            "w" => Streambugs.convert_to_values_and_input_indices(
                vcat(
                    repeat([Streambugs.ValueAndInputIndex(5.0, 0)], 18),
                    repeat([Streambugs.ValueAndInputIndex(10.0, 0)], 18),
                    repeat([Streambugs.ValueAndInputIndex(10.0, 1)], 18),
                ),
            ),
            "L" => Streambugs.convert_to_values_and_input_indices(
                vcat(repeat([Streambugs.ValueAndInputIndex(1000.0, 0)], 54)),
            ),
        )
    )
    @assert_equals(
        Streambugs.get_values_and_indices_reach(["DFish", "fA"], y_names, pars, inps),
        Dict(
            "DFish" => Streambugs.convert_to_values_and_input_indices(
                vcat(
                    repeat([Streambugs.ValueAndInputIndex(100.0, 0)], 18),
                    repeat([Streambugs.ValueAndInputIndex(200.0, 0)], 18),
                    repeat([Streambugs.ValueAndInputIndex(100.0, 0)], 18),
                ),
            ),
            "fA" => Streambugs.convert_to_values_and_input_indices(
                vcat(repeat([Streambugs.ValueAndInputIndex(NaN, 0)], 54)),
            ),
        )
    )
    @assert_equals(
        Streambugs.get_values_and_indices_reach(["T", "fA"], y_names, pars, inps, defaults),
        Dict(
            "T" => Streambugs.convert_to_values_and_input_indices(
                vcat(repeat([Streambugs.ValueAndInputIndex(293.15, 0)], 54)),
            ),
            "fA" => Streambugs.convert_to_values_and_input_indices(
                vcat(repeat([Streambugs.ValueAndInputIndex(3.5, 0)], 54)),
            ),
        )
    )
end

@testset "get_values_and_indices_habitat" begin
    model = Streambugs.streambugs_example_model_toy()
    y_names = model.y_names
    pars = model.parameters
    pars["fshade"] = 0.7
    inps = model.inputs
    @assert_equals(
        Streambugs.get_values_and_indices_habitat(
            ["w", "fA", "fshade"],
            y_names,
            pars,
            inps,
        ),
        Dict(
            "w" => Streambugs.convert_to_values_and_input_indices(
                vcat(
                    repeat([Streambugs.ValueAndInputIndex(5.0, 0)], 18),
                    repeat([Streambugs.ValueAndInputIndex(10.0, 0)], 18),
                    repeat([Streambugs.ValueAndInputIndex(10.0, 1)], 18),
                ),
            ),
            "fA" => Streambugs.ValuesAndInputIndices(
                repeat(Float64[NaN], 54),
                Tuple{Int,Int}[],
            ),
            "fshade" => Streambugs.convert_to_values_and_input_indices(
                vcat(
                    repeat([Streambugs.ValueAndInputIndex(0.2, 0)], 9),
                    repeat([Streambugs.ValueAndInputIndex(0.7, 0)], 9),
                    repeat([Streambugs.ValueAndInputIndex(0.2, 0)], 9),
                    repeat([Streambugs.ValueAndInputIndex(0.7, 0)], 9),
                    repeat([Streambugs.ValueAndInputIndex(0.2, 0)], 9),
                    repeat([Streambugs.ValueAndInputIndex(0.7, 0)], 9),
                ),
            ),
        )
    )

    pars["Hab2_T"] = 276.0
    pars["Reach3_Hab2_T"] = 280.0
    pars["Reach1_T"] = 290.0
    push!(inps, Streambugs.Input("Reach1_fA", 2.9))
    push!(inps, Streambugs.Input("Hab1_fA", 3.5))
    push!(inps, Streambugs.Input("Reach2_Hab1_fA", 9.2))
    defaults = Dict("fA" => 1.0, "Hab1_fA" => 3.0, "Reach3_Hab1_fA" => 7.0)
    @assert_equals(
        Streambugs.get_values_and_indices_habitat(
            ["T", "fA"],
            y_names,
            pars,
            inps,
            defaults,
        ),
        Dict(
            "T" => Streambugs.convert_to_values_and_input_indices(
                vcat(
                    repeat([Streambugs.ValueAndInputIndex(290.0, 0)], 9),
                    repeat([Streambugs.ValueAndInputIndex(276.0, 0)], 9),
                    repeat([Streambugs.ValueAndInputIndex(293.15, 0)], 9),
                    repeat([Streambugs.ValueAndInputIndex(276.0, 0)], 9),
                    repeat([Streambugs.ValueAndInputIndex(293.15, 0)], 9),
                    repeat([Streambugs.ValueAndInputIndex(280.0, 0)], 9),
                ),
            ),
            "fA" => Streambugs.convert_to_values_and_input_indices(
                vcat(
                    repeat([Streambugs.ValueAndInputIndex(0.75, 3)], 9),
                    repeat([Streambugs.ValueAndInputIndex(0.25, 2)], 9),
                    repeat([Streambugs.ValueAndInputIndex(0.75, 4)], 9),
                    repeat([Streambugs.ValueAndInputIndex(0.25, 0)], 9),
                    repeat([Streambugs.ValueAndInputIndex(0.875, 3)], 9),
                    repeat([Streambugs.ValueAndInputIndex(0.125, 0)], 9),
                ),
            ),
        )
    )
end

@testset "get_values_and_indices_habitat_group" begin
    model = Streambugs.streambugs_example_model_toy()
    y_names = model.y_names
    pars = model.parameters
    pars["Reach1_POM_Dini"] = 2.5
    inps = model.inputs
    push!(inps, Streambugs.Input("Hab1_POM_Dini", 12.5))
    @assert_equals(
        Streambugs.get_values_and_indices_habitat_group(["POM"], y_names, pars, inps),
        Dict(
            "POM" => Dict(
                "Dini" => Streambugs.convert_to_values_and_input_indices(
                    vcat(
                        repeat([Streambugs.ValueAndInputIndex(2.5, 2)], 9),
                        repeat([Streambugs.ValueAndInputIndex(2.5, 0)], 9),
                        repeat([Streambugs.ValueAndInputIndex(100.0, 2)], 9),
                        repeat([Streambugs.ValueAndInputIndex(100.0, 0)], 9),
                        repeat([Streambugs.ValueAndInputIndex(100.0, 2)], 9),
                        repeat([Streambugs.ValueAndInputIndex(100.0, 0)], 9),
                    ),
                ),
                "EC" => Streambugs.convert_to_values_and_input_indices(
                    repeat([Streambugs.ValueAndInputIndex(18000.0, 0)], 54),
                ),
                "cdet" => Streambugs.convert_to_values_and_input_indices(
                    repeat([Streambugs.ValueAndInputIndex(3.0, 0)], 54),
                ),
                "kminer" => Streambugs.convert_to_values_and_input_indices(
                    repeat([Streambugs.ValueAndInputIndex(20.0, 0)], 54),
                ),
            ),
        )
    )
    @assert_equals(
        Streambugs.get_values_and_indices_habitat_group(["ABCD"], y_names, pars, inps),
        Dict("ABCD" => Dict{String,Streambugs.ValuesAndInputIndices{Float64}}())
    )

end

@testset "get_values_and_indices_taxon_group" begin
    model = Streambugs.streambugs_example_model_toy()
    y_names = model.y_names
    pars = model.parameters
    inps = model.inputs
    push!(inps, Streambugs.Input("Invertebrates_M", 2.9))
    push!(inps, Streambugs.Input("Invert3_M", 3.5))
    push!(inps, Streambugs.Input("Ea", 9.2))
    defaults = Dict("POM_M" => 2.5, "POM1_M" => 4.5)
    @assert_equals(
        Streambugs.get_values_and_indices_taxon_group(
            ["Ea", "M"],
            y_names,
            pars,
            inps,
            defaults,
        ),
        Dict(
            "Ea" => Streambugs.convert_to_values_and_input_indices(
                repeat(
                    vcat(
                        repeat([Streambugs.ValueAndInputIndex(NaN, 4)], 2),
                        repeat([Streambugs.ValueAndInputIndex(0.231, 4)], 2),
                        repeat([Streambugs.ValueAndInputIndex(0.68642, 4)], 5),
                    ),
                    6,
                ),
            ),
            "M" => Streambugs.convert_to_values_and_input_indices(
                repeat(
                    vcat(
                        repeat([Streambugs.ValueAndInputIndex(4.5, 0)], 1),
                        repeat([Streambugs.ValueAndInputIndex(2.5, 0)], 1),
                        repeat([Streambugs.ValueAndInputIndex(1e-7, 0)], 2),
                        repeat([Streambugs.ValueAndInputIndex(1e-3, 2)], 2),
                        repeat([Streambugs.ValueAndInputIndex(1e-3, 3)], 1),
                        repeat([Streambugs.ValueAndInputIndex(1e-3, 2)], 2),
                    ),
                    6,
                ),
            ),
        )
    )
end

@testset "get_values_and_indices_taxon_group_reach_habitat" begin
    model = Streambugs.streambugs_example_model_toy()
    y_names = model.y_names
    pars = model.parameters
    inps = model.inputs
    pars["POM_Dini"] = 160.0
    pars["Input"] = 19.0
    pars["Reach2_Input"] = 16.0
    pars["Alga1_Reach2_Input"] = 6.0
    pars["Algae_Hab1_Input"] = 5.0
    pars["Invertebrates_Reach3_Hab2_Input"] = 4.0
    pars["Invert5_Reach3_Hab2_Input"] = 3.0
    pars["Invert5_Input"] = 2.0
    push!(inps, Streambugs.Input("Invert5_Input", 1.9))
    @assert_equals(
        Streambugs.get_values_and_indices_taxon_group_reach_habitat(
            ["Dini", "Input"],
            y_names["y_names"],
            pars,
            inps,
        ),
        Dict(
            "Dini" => Streambugs.convert_to_values_and_input_indices(
                repeat(
                    vcat(
                        repeat([Streambugs.ValueAndInputIndex(160.0, 0)], 2),
                        repeat([Streambugs.ValueAndInputIndex(100.0, 0)], 2),
                        repeat([Streambugs.ValueAndInputIndex(10.0, 0)], 5),
                    ),
                    6,
                ),
            ),
            "Input" => Streambugs.convert_to_values_and_input_indices(
                vcat(
                    repeat([Streambugs.ValueAndInputIndex(19.0, 0)], 2),
                    repeat([Streambugs.ValueAndInputIndex(5.0, 0)], 2),
                    repeat([Streambugs.ValueAndInputIndex(19.0, 0)], 4),
                    repeat([Streambugs.ValueAndInputIndex(2.0, 2)], 1),
                    repeat([Streambugs.ValueAndInputIndex(19.0, 0)], 8),
                    repeat([Streambugs.ValueAndInputIndex(2.0, 2)], 1),
                    repeat([Streambugs.ValueAndInputIndex(16.0, 0)], 2),
                    repeat([Streambugs.ValueAndInputIndex(5.0, 0)], 2),
                    repeat([Streambugs.ValueAndInputIndex(16.0, 0)], 4),
                    repeat([Streambugs.ValueAndInputIndex(2.0, 2)], 1),
                    repeat([Streambugs.ValueAndInputIndex(16.0, 0)], 2),
                    repeat([Streambugs.ValueAndInputIndex(6.0, 0)], 1),
                    repeat([Streambugs.ValueAndInputIndex(16.0, 0)], 5),
                    repeat([Streambugs.ValueAndInputIndex(2.0, 2)], 1),
                    repeat([Streambugs.ValueAndInputIndex(19.0, 0)], 2),
                    repeat([Streambugs.ValueAndInputIndex(5.0, 0)], 2),
                    repeat([Streambugs.ValueAndInputIndex(19.0, 0)], 4),
                    repeat([Streambugs.ValueAndInputIndex(2.0, 2)], 1),
                    repeat([Streambugs.ValueAndInputIndex(19.0, 0)], 4),
                    repeat([Streambugs.ValueAndInputIndex(4.0, 0)], 4),
                    repeat([Streambugs.ValueAndInputIndex(3.0, 2)], 1),
                ),
            ),
        )
    )
end

@testset "get_values_and_indices_taxaprop_traits" begin
    model = Streambugs.streambugs_example_model_toy()
    y_names = model.y_names
    pars = model.parameters
    inps = model.inputs
    pars["Algae_tempmaxKval_T"] = 2.75
    pars["POM_tempmaxKval_w"] = 2.75
    pars["POM_tempmaxtolval_w"] = 0.25
    pars["tempmaxtolval_w"] = 0.125
    pars["microhabaf_T"] = 0.5
    pars["microhabtolval_T"] = 0.05
    push!(inps, Streambugs.Input("POM_microhabtolval_T", 1.9))
    values_and_indices_microhabaf = Streambugs.get_values_and_indices_habitat_group(
        ["microhabaf"],
        y_names,
        pars,
        inps,
    )["microhabaf"]
    values_and_indices_tempmaxKval =
        Streambugs.get_values_and_indices_global_envcondtraits(["tempmaxKval"], pars, inps)["tempmaxKval"]
    @assert_equals(
        Streambugs.get_values_and_indices_taxaprop_traits(
            Dict(
                "microhabtolval" => values_and_indices_microhabaf,
                "tempmaxtolval" => values_and_indices_tempmaxKval,
            ),
            y_names,
            pars,
            inps,
        ),
        Dict(
            "microhabtolval" => Dict(
                "T" => Streambugs.convert_to_values_and_input_indices(
                    repeat(
                        vcat(
                            repeat([Streambugs.ValueAndInputIndex(0.05, 2)], 2),
                            repeat([Streambugs.ValueAndInputIndex(0.05, 0)], 7),
                        ),
                        6,
                    ),
                ),
            ),
            "tempmaxtolval" => Dict(
                "w" => Streambugs.convert_to_values_and_input_indices(
                    repeat(
                        vcat(
                            repeat([Streambugs.ValueAndInputIndex(0.25, 0)], 2),
                            repeat([Streambugs.ValueAndInputIndex(0.125, 0)], 7),
                        ),
                        6,
                    ),
                ),
                "T" => Streambugs.convert_to_values_and_input_indices(
                    repeat([Streambugs.ValueAndInputIndex(NaN, 0)], 54),
                ),
            ),
        )
    )
end

@testset "get_stoich_taxon" begin
    model = Streambugs.streambugs_example_model_toy()
    pars = model.parameters
    pars["Prod_Alga1_Alga2"] = -1
    @assert_equals(
        Streambugs.get_stoich_taxon("Prod", pars),
        Dict{String,Dict{String,Float64}}(
            "Alga1" => Dict("Alga1" => 1, "Alga2" => -1),
            "Alga2" => Dict("Alga2" => 1),
        )
    )
    @assert_equals Streambugs.get_stoich_taxon("unknown", pars) Dict{
        String,
        Dict{String,Float64},
    }()
    @test_throws(
        ArgumentError(
            "Parameter started with 'POM' does not have 3 components:" * " POM_cdet",
        ),
        Streambugs.get_stoich_taxon("POM", pars)
    )
end

@testset "get_stoich_web" begin
    model = Streambugs.streambugs_example_model_toy()
    pars = model.parameters
    pars["FishPred_Fish_Invert5_POM1"] = 1
    @assert_equals(
        Streambugs.get_stoich_web("FishPred", pars),
        Dict{String,Dict{String,Dict{String,Float64}}}(
            "Fish" => Dict(
                "Invert1" => Dict("Invert1" => -1),
                "Invert5" => Dict("Invert5" => -1, "POM1" => 1),
            ),
        )
    )
    @assert_equals(
        Streambugs.get_stoich_web("unknown", pars),
        Dict{String,Dict{String,Dict{String,Float64}}}()
    )
    @test_throws(
        ArgumentError(
            "Parameter started with 'Prod' does not have 4 components:" *
            " Prod_Alga1_Alga1",
        ),
        Streambugs.get_stoich_web("Prod", pars)
    )
end

@testset "get_par_proc_taxon" begin
    # Given
    model = Streambugs.streambugs_example_model_toy()
    y_names = model.y_names
    pars = model.parameters
    inps = model.inputs
    push!(inps, Streambugs.Input("Algae_fprod", 1.9))
    push!(inps, Streambugs.Input("Alga1_Reach1_Hab2_KP", 2.9))
    kinparnames_taxon = Dict(
        "Death" => ["fdeath"],
        "Prod" => ["fprod", "fgrotax", "KP", "KN"],
        "Drift" => ["cdet"],
    )
    stoich_taxon = Dict{String,Dict{String,Dict{String,Float64}}}()
    for proc in keys(kinparnames_taxon)
        stoich_taxon[proc] = Streambugs.get_stoich_taxon(proc, pars)
    end
    y_name_indices = Streambugs.get_y_names_indices_by_name_hierarchy(y_names["y_names"])

    # When
    par_proc_taxon = Streambugs.get_par_proc_taxon(
        kinparnames_taxon,
        stoich_taxon,
        y_name_indices,
        y_names["y_names"],
        pars,
        inps,
    )

    # Then
    @assert_equals(
        par_proc_taxon["Reach2_Hab1_Alga1_Algae"],
        [
            Streambugs.StoichInfo(
                "Prod",
                Dict(
                    "fprod" => Streambugs.ValueAndInputIndex(5.0, 2),
                    "fgrotax" => Streambugs.ValueAndInputIndex(1.0, 0),
                    "KP" => Streambugs.ValueAndInputIndex(0.01, 0),
                    "KN" => Streambugs.ValueAndInputIndex(0.1, 0),
                ),
                [Streambugs.StateVariableAndCoeff(21, 1.0)],
                0,
            ),
            Streambugs.StoichInfo(
                "Drift",
                Dict("cdet" => Streambugs.ValueAndInputIndex(2.0, 0)),
                [Streambugs.StateVariableAndCoeff(21, -1.0)],
                0,
            ),
            Streambugs.StoichInfo(
                "Death",
                Dict("fdeath" => Streambugs.ValueAndInputIndex(0.7, 0)),
                [
                    Streambugs.StateVariableAndCoeff(21, -1.0),
                    Streambugs.StateVariableAndCoeff(19, 1.0),
                ],
                0,
            ),
        ]
    )
    @assert_equals(
        par_proc_taxon["Reach1_Hab2_Alga1_Algae"],
        [
            Streambugs.StoichInfo(
                "Prod",
                Dict(
                    "fprod" => Streambugs.ValueAndInputIndex(5.0, 2),
                    "fgrotax" => Streambugs.ValueAndInputIndex(1.0, 0),
                    "KP" => Streambugs.ValueAndInputIndex(0.01, 3),
                    "KN" => Streambugs.ValueAndInputIndex(0.1, 0),
                ),
                [Streambugs.StateVariableAndCoeff(12, 1.0)],
                0,
            ),
            Streambugs.StoichInfo(
                "Drift",
                Dict("cdet" => Streambugs.ValueAndInputIndex(2.0, 0)),
                [Streambugs.StateVariableAndCoeff(12, -1.0)],
                0,
            ),
            Streambugs.StoichInfo(
                "Death",
                Dict("fdeath" => Streambugs.ValueAndInputIndex(0.7, 0)),
                [
                    Streambugs.StateVariableAndCoeff(12, -1.0),
                    Streambugs.StateVariableAndCoeff(10, 1.0),
                ],
                0,
            ),
        ]
    )
end

@testset "get_par_proc_web" begin
    # Given
    model = Streambugs.streambugs_example_model_toy()
    y_names = model.y_names
    pars = model.parameters
    inps = model.inputs
    pars["aaa"] = 2.5
    pars["Fish_Reach1_aaa"] = 5.25
    pars["Fish_Invert1_Reach1_aaa"] = 99.5
    pars["Fish_Invertebrates_Reach1_aaa"] = -7.5
    pars["Cons_POM1_Alga1_POM2"] = 1.0
    push!(inps, Streambugs.Input("Fish_Kfood", 1.9))
    push!(inps, Streambugs.Input("Fish_Hab1_aaa", 2.0))
    push!(inps, Streambugs.Input("POM_Hab2_hdens", 2.1))
    push!(inps, Streambugs.Input("Fish_Invert5_Hab2_aaa", 2.2))
    kinparnames_web = Dict(
        "Cons" => Dict(
            "taxon1" => ["fcons", "fgrotax", "hdens", "Kfood", "q"],
            "taxa2" => ["Pref"],
        ),
        "FishPred" =>
            Dict("taxon1" => ["cfish", "Kfood", "q"], "taxa2" => ["Pref", "aaa"]),
    )
    stoich_web = Dict{String,Dict{String,Dict{String,Dict{String,Float64}}}}()
    for proc in keys(kinparnames_web)
        stoich_web[proc] = Streambugs.get_stoich_web(proc, pars)
    end
    y_name_indices = Streambugs.get_y_names_indices_by_name_hierarchy(y_names["y_names"])

    # When
    par_proc_web = Streambugs.get_par_proc_web(
        kinparnames_web,
        stoich_web,
        y_name_indices,
        y_names["y_names"],
        y_names["indfA"],
        pars,
        inps,
    )

    # Then
    @assert_equals(
        par_proc_web["Reach1_Hab2_POM1_POM"],
        [
            Dict(
                "Cons" => Streambugs.ProcWebInfo(
                    Dict(
                        "fcons" => Streambugs.ValueAndInputIndex(NaN, 0),
                        "fgrotax" => Streambugs.ValueAndInputIndex(1.0, 0),
                        "hdens" => Streambugs.ValueAndInputIndex(NaN, 4),
                        "Kfood" => Streambugs.ValueAndInputIndex(NaN, 0),
                        "q" => Streambugs.ValueAndInputIndex(1.0, 0),
                    ),
                    [
                        Streambugs.StoichInfo(
                            "Alga1",
                            Dict("Pref" => Streambugs.ValueAndInputIndex(1.0, 0)),
                            [Streambugs.StateVariableAndCoeff(11, 1.0)],
                            0,
                        ),
                    ],
                ),
            ),
            Dict(
                "FishPred" => Streambugs.ProcWebInfo(
                    Dict(
                        "cfish" => Streambugs.ValueAndInputIndex(10.0, 0),
                        "Kfood" => Streambugs.ValueAndInputIndex(1.0, 2),
                        "q" => Streambugs.ValueAndInputIndex(1.0, 0),
                    ),
                    [
                        Streambugs.StoichInfo(
                            "Invert1",
                            Dict(
                                "Pref" => Streambugs.ValueAndInputIndex(1.0, 0),
                                "aaa" => Streambugs.ValueAndInputIndex(99.5, 0),
                            ),
                            [Streambugs.StateVariableAndCoeff(14, -1.0)],
                            14,
                        ),
                        Streambugs.StoichInfo(
                            "Invert5",
                            Dict(
                                "Pref" => Streambugs.ValueAndInputIndex(1.0, 0),
                                "aaa" => Streambugs.ValueAndInputIndex(-7.5, 5),
                            ),
                            [Streambugs.StateVariableAndCoeff(18, -1.0)],
                            18,
                        ),
                    ],
                ),
            ),
        ]
    )
    @assert_equals(
        par_proc_web["Reach1_Hab2_Invert2_Invertebrates"],
        [
            Dict(
                "Cons" => Streambugs.ProcWebInfo(
                    Dict(
                        "fgrotax" => Streambugs.ValueAndInputIndex(1.0, 0),
                        "Kfood" => Streambugs.ValueAndInputIndex(1.0, 0),
                        "fcons" => Streambugs.ValueAndInputIndex(5.0, 0),
                        "hdens" => Streambugs.ValueAndInputIndex(10.0, 0),
                        "q" => Streambugs.ValueAndInputIndex(2.0, 0),
                    ),
                    [
                        Streambugs.StoichInfo(
                            "Alga1",
                            Dict("Pref" => Streambugs.ValueAndInputIndex(1.0, 0)),
                            [
                                Streambugs.StateVariableAndCoeff(12, -1.0),
                                Streambugs.StateVariableAndCoeff(15, 1.0),
                            ],
                            12,
                        ),
                        Streambugs.StoichInfo(
                            "POM1",
                            Dict("Pref" => Streambugs.ValueAndInputIndex(1.0, 0)),
                            [
                                Streambugs.StateVariableAndCoeff(15, 1.0),
                                Streambugs.StateVariableAndCoeff(10, -1.0),
                            ],
                            10,
                        ),
                    ],
                ),
            ),
        ]
    )
end

@testset "get_par_trait_cond" begin
    # Given
    model = Streambugs.streambugs_example_model_toy()
    model.parameters["saprowqclassval_class0"] = 1
    model.parameters["saprowqclassval_class1"] = 2
    model.parameters["saprotolval_class0"] = 10
    model.parameters["saprotolval_class1"] = 20
    model.parameters["Algae_saprotolval_class1"] = 30
    model.parameters["Alga1_saprotolval_class1"] = 40
    model.parameters["saprowqclass"] = 1.6
    model.parameters["fsapro_intercept"] = 6
    model.parameters["fsapro_curv"] = 0.001
    model.parameters["microhabaf_type1"] = 2
    model.parameters["microhabaf_type2"] = 3
    model.parameters["POM_microhabtolval_type1"] = 1.2
    model.parameters["POM_microhabtolval_type2"] = 5
    model.parameters["fmicrohab_intercept"] = 10
    model.parameters["fmicrohab_curv"] = 0.1
    # When
    par_trait_cond = Streambugs.SystemDefinition(model).par_trait_cond
    # Then
    @assert_equals(
        par_trait_cond,
        Streambugs.TraitCondition{Float64}(
            repeat([1.0], 54),
            repeat(vcat(repeat([-83.0260703900785], 2), repeat([1], 7)), 6),
            repeat([1.0], 54),
            repeat(
                vcat(
                    repeat([-73.40308803705504], 2),
                    [-132.1272065816138, -102.85323322289359],
                    repeat([-73.40308803705504], 5),
                ),
                6,
            ),
            repeat([1.0], 54),
        )
    )
end

@testset "differentiation of expt() at curv" begin
    function testfunc(x)
        Streambugs.expt(x[1] * x[2], sin(x[1] + x[2]))
    end
    function poor_mans_gradient(x)
        ∆ = 0.001
        result = Float64[]
        for i = 1:2
            xdx = copy(x)
            xdx[i] += ∆
            push!(result, (testfunc(xdx) - testfunc(x)) / ∆)
        end
        return result
    end
    @test ForwardDiff.gradient(testfunc, [0.1, -0.1]) ≈ poor_mans_gradient([0.1, -0.1]) rtol =
        1e-2
    @test ForwardDiff.gradient(testfunc, [0.1, 0.1]) ≈ poor_mans_gradient([0.1, 0.1]) rtol =
        1e-2
end

@testset "streambugs_rhs" begin
    # Given
    model = Streambugs.streambugs_example_model_toy()
    model.parameters["saprowqclassval_class0"] = 1.0
    model.parameters["saprowqclassval_class1"] = 2.0
    model.parameters["saprotolval_class0"] = 10.0
    model.parameters["saprotolval_class1"] = 20.0
    model.parameters["Algae_saprotolval_class1"] = 30.0
    model.parameters["Alga1_saprotolval_class1"] = 40.0
    model.parameters["saprowqclass"] = 1.6
    model.parameters["fsapro_intercept"] = 6.0
    model.parameters["fsapro_curv"] = 0.001
    model.parameters["microhabaf_type1"] = 2.0
    model.parameters["microhabaf_type2"] = 3.0
    model.parameters["POM_microhabtolval_type1"] = 1.2
    model.parameters["POM_microhabtolval_type2"] = 5.0
    model.parameters["fmicrohab_intercept"] = 10.0
    model.parameters["fmicrohab_curv"] = 0.1
    push!(model.inputs, Streambugs.Input("Hab2_tau", 1.5))
    sd = Streambugs.SystemDefinition(model)
    y_ini =
        Streambugs.get_values(sd.inputs, sd.par_taxon_group_reach_habitat["Dini"], 0.0) .*
        Streambugs.get_values(sd.inputs, sd.par_env["fA"], 0.0) .*
        Streambugs.get_values(sd.inputs, sd.par_env["w"], 0.0)
    dydt = ones(length(sd.y_names["y_names"]))
    # When
    Streambugs.streambugs_rhs(dydt, y_ini, sd, 0.1)
    # Then
    rounded_dydt = Vector{String}()
    for s in dydt
        push!(rounded_dydt, @sprintf("%2.3f", s))
    end
    @assert_equals(
        rounded_dydt,
        [
            "-2865056.833",
            "-5000.000",
            "1593730.493",
            "1243767.153",
            "8307.124",
            "8350.606",
            "8350.606",
            "8350.606",
            "8307.124",
            "-2865244.333",
            "-5187.500",
            "1593605.493",
            "1243642.153",
            "8300.874",
            "8344.356",
            "8344.356",
            "8344.356",
            "8300.874",
            "-5730113.666",
            "-10000.000",
            "3187460.986",
            "2487534.305",
            "16527.284",
            "16701.213",
            "16701.213",
            "16701.213",
            "16527.284",
            "-5730488.666",
            "-10375.000",
            "3187210.986",
            "2487284.305",
            "16514.784",
            "16688.713",
            "16688.713",
            "16688.713",
            "16514.784",
            "-5730029.030",
            "-10000.000",
            "3187110.477",
            "2487099.161",
            "16587.045",
            "16667.358",
            "16667.358",
            "16667.358",
            "16587.045",
            "-5730404.030",
            "-10375.000",
            "3186860.477",
            "2486849.161",
            "16574.545",
            "16654.858",
            "16654.858",
            "16654.858",
            "16574.545",
        ]
    )
end

@testset "streambugs_rhs for extended example" begin
    # Given
    model = Streambugs.streambugs_example_model_extended()
    sd = Streambugs.SystemDefinition(model)
    y_ini =
        Streambugs.get_values(sd.inputs, sd.par_taxon_group_reach_habitat["Dini"], 0.0) .*
        Streambugs.get_values(sd.inputs, sd.par_env["fA"], 0.0) .*
        Streambugs.get_values(sd.inputs, sd.par_env["w"], 0.0)
    dydt = ones(length(sd.y_names["y_names"]))
    # When
    Streambugs.streambugs_rhs(dydt, y_ini, sd, 0.1)
    # Then
    expected_dydt = [
        2275.56443606087,
        1875.12148885822,
        -355.569972211802,
        -355.569972211802,
        -51.6956155343226,
        -24.1712599808638,
        -51.9142120886453,
        -19.3188563803288,
        -186.901179675623,
        -178.739278880752,
        -50.9228298874333,
        -157.50431706929,
        -206.523092039819,
        -78.4357902589164,
        -45.009015685367,
        -180.496344970139,
        -173.554935530774,
        -109.146554674768,
        -47.4561970267741,
        -114.460038153244,
        -55.133776595634,
        -24.4289690036378,
        -24.5533122108985,
        -13.3390529729206,
        -89.8844307351283,
        -69.1877471566822,
        -35.2923803892868,
        -26.0384332391514,
        -98.9273993572841,
        -77.2617082422425,
        -50.6727733440766,
        -50.2339762982943,
        -236.345303422897,
        -79.527421691915,
        -61.0467718102305,
        -174.62460595656,
        -136.901211235756,
        -110.088887017909,
        -47.6693016114401,
        3525.42879665988,
        233.268110047099,
        131.807290076157,
        131.807290076157,
        -80.6311977009447,
        -37.5647405232538,
        -80.6824102395293,
        -30.0235828710458,
        -289.782798713377,
        -277.801201041791,
        -79.1781788313056,
        -244.799788512999,
        -320.593135673579,
        -121.936281561554,
        -69.9509915482152,
        -280.59164430907,
        -269.720256844441,
        -169.0663091088,
        -73.7541748401117,
        -177.904335504475,
        -85.7049464086729,
        -37.965248092105,
        -38.1584908241876,
        -20.7303245321262,
        -139.711204534932,
        -107.527343572084,
        -54.8502941106256,
        -40.4665288046699,
        -153.526594583601,
        -120.07514842024,
        -78.7720670407453,
        -78.0160497020179,
        -367.327165329744,
        -123.71207409088,
        -94.8943776227433,
        -271.252811758273,
        -212.398407381715,
        -171.111105006689,
        -73.8297612487442,
        3791.1841665256,
        2497.24225323106,
        -252.21793096264,
        -252.21793096264,
        -89.5317748924655,
        -33.2104187480803,
        -73.5039496261636,
        -32.2290578549433,
        -313.44865209738,
        -291.179258902588,
        -80.4361082264153,
        -263.579921633503,
        -352.064296096012,
        -133.925268512868,
        -69.3279006973022,
        -304.597645409408,
        -293.761615657235,
        -186.153766598962,
        -73.6358790240153,
        -194.256327143201,
        -87.687920843925,
        -38.0591357504822,
        -36.5971128458084,
        -21.3898171929581,
        -150.237102492978,
        -111.390522109356,
        -42.0349562012734,
        -43.439122626906,
        -158.980055155372,
        -116.456656884601,
        -82.7355571621272,
        -73.2254845639643,
        -390.231431060911,
        -141.722347907572,
        -107.634960905563,
        -290.569045283075,
        -229.869944077448,
        -160.467067640254,
        -77.7709601897255,
        2000.60630076335,
        874.018877913395,
        -180.332110313733,
        -180.332110313733,
        -41.5610447416412,
        -22.6184916719054,
        -26.4698267115222,
        -26.2172812127905,
        -107.589458982616,
        -73.9920654237569,
        -33.9896460402162,
        -147.713792786353,
        -132.707540560767,
        -70.2943265437768,
        -54.3588826020288,
        -105.488358113519,
        -76.2876516796234,
        -116.6554041201,
        -31.6202541914308,
        -192.716244496847,
        -36.7656316723663,
        -16.2752854401409,
        -20.0841019549797,
        -12.7737038349645,
        -70.1089851625685,
        -62.1720034322143,
        -43.98070994721,
        -31.576240459737,
        -190.868041527672,
        -143.371282007721,
        -38.1157157707662,
        -35.0140048075805,
        -76.1529113041434,
        -63.9359447082978,
        -30.9850013819446,
        -91.8053741814947,
        -177.105974090605,
        -73.3783150968614,
        -53.6712954036146,
        2273.82547074789,
        1886.3128194166,
        -213.181689094787,
        -213.181689094787,
        -54.4852267489694,
        -19.8374642365801,
        -43.9221422520048,
        -19.2512713382472,
        -184.135190305673,
        -173.141050151377,
        -48.2325741313336,
        -157.572334187947,
        -209.209940314747,
        -80.1830687118493,
        -41.4276774304408,
        -182.257772609686,
        -175.476562002527,
        -108.734506578453,
        -44.0009471816111,
        -114.44994661623,
        -52.5073754582272,
        -22.7337315452843,
        -21.8604264748325,
        -12.776705310794,
        -89.8696611735246,
        -66.5527971793944,
        -25.124877563389,
        -25.9473404450667,
        -93.2086611499475,
        -69.5789338922122,
        -49.54919723803,
        -43.3356416965471,
        -232.134972395738,
        -85.0876869395723,
        -64.4222717096107,
        -172.980027888052,
        -135.803969243243,
        -95.9802907711192,
        -45.425024548975,
        1156.34152432459,
        2076.78107167533,
        -206.35946503966,
        -206.35946503966,
        -59.6279634320336,
        -26.7151612612902,
        -28.7196274824142,
        -35.4137638628576,
        -47.119873183621,
        -43.5199190767182,
        -43.7983926478169,
        -67.0105149122943,
        -59.9501613772312,
        -96.9324135890333,
        -70.3530514433198,
        -50.0923178809966,
        -42.0385185003281,
        -54.6028321457229,
        -39.7597806020951,
        -75.0709952856833,
        -47.5739085979982,
        -20.5305524668245,
        -24.7748524171283,
        -16.7887498661594,
        -95.0817462977357,
        -81.8379151408325,
        -50.3558471131406,
        -42.6525357164949,
        -63.0734857766015,
        -42.1605706768545,
        -50.7406632404973,
        -40.7723225595011,
        -46.1268696501392,
        -91.9488115403353,
        -45.2044762308782,
        -42.761330840659,
        -67.0630271029384,
        -36.4536490992369,
        -69.7676436229322,
    ]
    for i = 1:length(expected_dydt)
        @test vcat([i], dydt[i]) ≈ vcat([i], expected_dydt[i]) rtol = 1e-7 atol = 1e-7
    end
end

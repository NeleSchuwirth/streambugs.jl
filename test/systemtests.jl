@testset "solve toy model" begin
    # Given
    model = Streambugs.streambugs_example_model_toy()
    # When
    result =
        Streambugs.run_streambugs(model, Vector(0:200) / 100; reltol = 1e-5, abstol = 1e-5)
    # Then
    expected_result =
        split(read(open("test/expected_toy_model_result.csv"), String), "\n")[2:end]
    for i = 1:length(result.t)
        t = result.t[i]
        expected_row = map(c -> parse(Float64, c), split(expected_result[i], ",")[2:end])
        for j = 1:length(expected_row)
            @test vcat([t, j], result.u[i][j]) ≈ vcat([t, j], expected_row[j]) rtol = 1e-3 atol =
                1e-3
        end
    end
end

@testset "solve extended example" begin
    # Given
    model = Streambugs.streambugs_example_model_extended()
    # When
    result =
        Streambugs.run_streambugs(model, Vector(0:200) / 20; reltol = 1e-5, abstol = 1e-5)
    # Then
    expected_result = read(open("test/expected_extended_example_result.csv"), String)
    expected_result = split(expected_result, "\n")[2:end]
    for i = 1:length(result.t)
        t = result.t[i]
        expected_row = map(c -> parse(Float64, c), split(expected_result[i], ",")[2:end])
        for j = 1:length(expected_row)
            @test vcat([t, j], result.u[i][j]) ≈ vcat([t, j], expected_row[j]) rtol = 1e-3 atol =
                1e-3
        end
    end
end


@testset "automatic differentiation example for the toy model" begin
    # Given
    model = Streambugs.streambugs_example_model_toy()
    times = [0, 0.5, 1.0]
    y_ini = Streambugs.get_y_ini(model)
    par_pairs = sort(collect(model.parameters))
    parscount = length(par_pairs)
    println("$(parscount) parameters")
    par_names = getproperty.(par_pairs, :first)
    run_function =
        (parameter_values; kwargs...) -> run_streambugs(
            model,
            times;
            parameter_names = par_names,
            parameter_values,
            y_ini,
            reltol = 1e-5,
            abstol = 1e-5,
            kwargs...,
        )
    function squared_deviation_function(x, dt = nothing)
        observed = [250, 473, 424]
        solution = run_function(x; dt = dt)
        sum(squared_deviation(observed, solution, 1))
    end
    x = getproperty.(par_pairs, :second)

    # When
    @time actual_gradient = ForwardDiff.gradient(squared_deviation_function, x)
    @test any(abs.(actual_gradient) .> 0.0)
    @time actual_gradient = ForwardDiff.gradient(squared_deviation_function, x)
    @test any(abs.(actual_gradient) .> 0.0)
    println("in-place")
    cfg = ForwardDiff.GradientConfig(squared_deviation_function, x)
    actual_gradient_ip = similar(x)
    @time ForwardDiff.gradient!(actual_gradient_ip, squared_deviation_function, x, cfg)
    @test any(abs.(actual_gradient_ip) .> 0.0)
    @time ForwardDiff.gradient!(actual_gradient_ip, squared_deviation_function, x, cfg)
    @test any(abs.(actual_gradient_ip) .> 0.0)

    # Then
    dt = 0.01
    l = squared_deviation_function(x, dt)
    println(l)
    ∆ = 1e-7
    for i = 1:length(x)
        xdx = copy(x)
        dx = xdx[i] * ∆
        xdx[i] += dx
        poor_mans_gradient = (squared_deviation_function(xdx, dt) - l) / dx
        success = isapprox(actual_gradient[i], poor_mans_gradient, rtol = 0.01, atol = 0.01)
        if success == false
            println(
                "x[$(i)]=$(x[i]), dx=$(dx), $(actual_gradient[i]) !≈ $(poor_mans_gradient)" *
                "\t($(par_names[i]))",
            )
        end
        @test success
        success_ip =
            isapprox(actual_gradient_ip[i], poor_mans_gradient, rtol = 0.01, atol = 0.01)
        if success_ip == false
            println(
                "x[$(i)]=$(x[i]), dx=$(dx), $(actual_gradient_ip[i]) !≈ $(poor_mans_gradient)" *
                "\t($(par_names[i]))",
            )
        end
        @test success_ip
    end
end


@testset "automatic differentiation example for the extended example" begin
    # Given
    model = Streambugs.streambugs_example_model_extended()
    times = [0, 0.5, 1.0]
    y_ini = Streambugs.get_y_ini(model)
    par_pairs = sort(collect(remove_fixed_pars(model.parameters))[1:50])
    parscount = length(par_pairs)
    println("$(parscount) parameters (instead of $(length(model.parameters)))")
    par_names = getproperty.(par_pairs, :first)
    println(par_names)
    run_function =
        (parameter_values; kwargs...) -> run_streambugs(
            model,
            times;
            parameter_names = par_names,
            parameter_values,
            y_ini,
            reltol = 1e-5,
            abstol = 1e-5,
            kwargs...,
        )
    function squared_deviation_function(x, dt = nothing)
        observed = [4, 84, 93]
        solution = run_function(x; dt = dt)
        sum(squared_deviation(observed, solution, 1))
    end
    x = getproperty.(par_pairs, :second)

    # When
    @time actual_gradient = ForwardDiff.gradient(squared_deviation_function, x)
    @test any(abs.(actual_gradient) .> 0.0)
    @time actual_gradient = ForwardDiff.gradient(squared_deviation_function, x)
    @test any(abs.(actual_gradient) .> 0.0)
    println("in-place")
    cfg = ForwardDiff.GradientConfig(squared_deviation_function, x)
    actual_gradient_ip = similar(x)
    @time ForwardDiff.gradient!(actual_gradient_ip, squared_deviation_function, x, cfg)
    @test any(abs.(actual_gradient_ip) .> 0.0)
    @time ForwardDiff.gradient!(actual_gradient_ip, squared_deviation_function, x, cfg)
    @test any(abs.(actual_gradient_ip) .> 0.0)

    # Then
    dt = 0.01
    l = squared_deviation_function(x, dt)
    println(l)
    ∆ = 1e-4
    for i = 1:length(x)
        xdx = copy(x)
        dx = (xdx[i] == 0 ? 1 : xdx[i]) * ∆
        xdx[i] += dx
        poor_mans_gradient = (squared_deviation_function(xdx, dt) - l) / dx
        success = isapprox(actual_gradient[i], poor_mans_gradient, rtol = 0.01, atol = 0.01)
        if success == false
            println(
                "x[$(i)]=$(x[i]), dx=$(dx), $(actual_gradient[i]) !≈ $(poor_mans_gradient)" *
                "\t($(par_names[i]))",
            )
        end
        @test success
        success_ip =
            isapprox(actual_gradient_ip[i], poor_mans_gradient, rtol = 0.01, atol = 0.01)
        if success_ip == false
            println(
                "x[$(i)]=$(x[i]), dx=$(dx), $(actual_gradient_ip[i]) !≈ $(poor_mans_gradient)" *
                "\t($(par_names[i]))",
            )
        end
        @test success_ip
    end
end

function print_pretty(benchmarkgroup::BenchmarkGroup; indent = "")
    for key in keys(benchmarkgroup)
        println("$(indent)$(key):")
        print_pretty(benchmarkgroup[key], indent = indent * "   ")
    end
end

function print_pretty(trial::BenchmarkTools.Trial; indent = "")
    rendered_time = string(BenchmarkTools.prettytime(minimum(trial).time))
    rendered_memory = string(BenchmarkTools.prettymemory(memory(trial)))
    println("$(indent)$(rendered_time), $(rendered_memory)")
end

toy_model = Streambugs.streambugs_example_model_toy()
toy_sys_def = Streambugs.SystemDefinition(toy_model)
toy_y_ini = Streambugs.get_y_ini(toy_model)
toy_par_pairs = sort(collect(toy_model.parameters))
toy_parscount = length(toy_par_pairs)
toy_par_names = getproperty.(toy_par_pairs, :first)
toy_run_function =
    parameter_values -> run_streambugs(
        toy_model,
        [0, 0.5, 1.0];
        y_ini = toy_y_ini,
        parameter_names = toy_par_names,
        parameter_values,
        reltol = 1e-5,
        abstol = 1e-5,
    )
function toy_test_function(x)
    observed = [250, 473, 424]
    solution = toy_run_function(x)
    sum(squared_deviation(observed, solution, 1))
end
toy_x = getproperty.(toy_par_pairs, :second)

suite = BenchmarkGroup()
suite["toy model"] = BenchmarkGroup()
suite["toy model"]["get_sys_def"] = @benchmarkable begin
    Streambugs.SystemDefinition($toy_model)
end
suite["toy model"]["run without get_sys_def"] = @benchmarkable begin
    Streambugs._run_streambugs($toy_sys_def, $toy_y_ini, Vector(0:200) / 100)
end
suite["toy model"]["run"] = @benchmarkable begin
    Streambugs.run_streambugs($toy_model, Vector(0:200) / 100)
end
suite["toy model"]["run automatic differentiation example"] = @benchmarkable begin
    local f = $toy_test_function
    local x = $toy_x
    local dx = similar(x)
    local cfg = ForwardDiff.GradientConfig(f, x)
    ForwardDiff.gradient!(dx, f, x, cfg)
end

model = Streambugs.streambugs_example_model_extended()
sys_def = Streambugs.SystemDefinition(model)
y_ini = Streambugs.get_y_ini(model)
par_pairs = sort(collect(remove_fixed_pars(model.parameters))[1:50])
# par_pairs = sort(collect(filter(par -> endswith(par.first, "_fgrotax"), model.parameters)))
# par_pairs = sort(collect(filter(par -> endswith(par.first, r"_fgrotax|_fbasaltax"), model.parameters)))
parscount = length(par_pairs)
par_names = getproperty.(par_pairs, :first)
run_function =
    parameter_values -> run_streambugs(
        model,
        [0.0, 0.27, 0.54, 0.81, 1.08, 1.35, 1.62, 1.89, 2.16, 2.43, 2.7];
        y_ini,
        parameter_names = par_names,
        parameter_values,
        reltol = 1e-5,
        abstol = 1e-5,
    )
function test_function(x)
    observed = [4.0, 78.0, 85.0, 90.0, 95.0, 100.0, 105.0, 110.0, 116.0, 121.0, 126.0]
    solution = run_function(x)
    sum(squared_deviation(observed, solution, 1))
end
x = getproperty.(par_pairs, :second)

suite["extended example"] = BenchmarkGroup()
suite["extended example"]["get_sys_def"] = @benchmarkable begin
    Streambugs.SystemDefinition($model)
end
suite["extended example"]["run without get_sys_def"] = @benchmarkable begin
    Streambugs._run_streambugs($sys_def, $y_ini, 2.7 * collect(0:0.01:1))
end
suite["extended example"]["run"] = @benchmarkable begin
    Streambugs.run_streambugs($model, 2.7 * collect(0:0.01:1))
end
suite["extended example"]["run automatic differentiation example"] = @benchmarkable begin
    local f = $test_function
    local x = $x
    local dx = similar(x)
    local cfg = ForwardDiff.GradientConfig(f, x)
    ForwardDiff.gradient!(dx, f, x, cfg)
end
suite["extended example"]["run automatic differentiation example - 3x rand perturb"] =
    @benchmarkable begin
        local f = $test_function
        local x = $x
        local dx = similar(x)
        local cfg = ForwardDiff.GradientConfig(f, x)
        for i = 1:3
            x = x + x .* 0.05 .* rand(length(x))
            ForwardDiff.gradient!(dx, f, x, cfg)
        end
    end


println("start tuning benchmark suite ...")
@time tune!(suite)
println("tuning benchmark suite finished")

result = run(suite)
print_pretty(result)

"""
    @assert_equals(actual::String, expected::String)

Compares two strings and show a diff in case of differences.
"""
macro assert_equals(actual, expected)
    return quote
        local diff =
            calculate_diff(render_pretty($(esc(actual))), render_pretty($(esc(expected))))
        if diff != ""
            println(diff)
            @test false
        end
        @test true
    end
end

function calculate_diff(actual::String, expected::String)
    function as_lines(string::String)
        lines = split(string, "\n")
        if length(lines[end]) == 0
            pop!(lines)
        end
        return lines
    end
    expected_lines = as_lines(expected)
    actual_lines = as_lines(actual)
    report = ""
    for i = 1:min(length(actual_lines), length(expected_lines))
        if actual_lines[i] != expected_lines[i]
            report *= "$(i) expected: >$(expected_lines[i])<\n"
            report *= "$(i)   actual: >$(actual_lines[i])<\n"
        end
    end
    if length(expected_lines) < length(actual_lines)
        report *= "The following lines are not expected\n"
        for i = length(expected_lines)+1:length(actual_lines)
            report *= "$(i): >$(actual_lines[i])<\n"
        end
    end
    if length(expected_lines) > length(actual_lines)
        report *= "The following lines are missing\n"
        for i = length(actual_lines)+1:length(expected_lines)
            report *= "$(i): >$(expected_lines[i])<\n"
        end
    end
    return report
end

function render_pretty(object)
    io = IOBuffer()
    print_pretty(io, object)
    return String(take!(io))
end

function render_pretty(model::Streambugs.Model)
    io = IOBuffer()
    println(io, "name:", model.name)
    print(io, "input:")
    print_pretty(io, model.inputs, "  ")
    print(io, "parameters:")
    print_pretty(io, model.parameters, "  ")
    print(io, "y_names:")
    print_pretty(io, model.y_names, "  ")
    return String(take!(io))
end

function print_pretty(io::IO, object, indent = "")
    if isa(object, Dict)
        println(io, "")
        for key in sort(collect(keys(object)))
            print(io, "$(indent)$(key):")
            print_pretty(io, object[key], indent * "  ")
        end
    elseif isa(object, Vector)
        println(io, "")
        for i = 1:length(object)
            print(io, "$(indent)$(i):")
            print_pretty(io, object[i], indent * "  ")
        end
    else
        println(io, "$(object)")
    end
end

function remove_fixed_pars(pars)
    stoich_prefixes = Set(["Miner", "Drift", "Resp", "Death", "Prod", "Cons", "FishPred"])
    function accept(e)
        terms = split(e.first, "_")
        (
            terms[1] ∉ stoich_prefixes &&
            (startswith(terms[end], "class") == false) &&
            (startswith(terms[end], "type") == false)
        )
    end
    filter(accept, pars)
end

function squared_deviation(observed::Vector{<:Real}, solution, y_index::Int)
    sum((solution[y_index, :] .- observed) .^ 2)
end

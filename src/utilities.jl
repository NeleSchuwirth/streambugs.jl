using Base.Iterators

"""
Implementation of R function 'paste'.
"""
function paste(items...; sep = " ", collapse = nothing)
    paste1(items, sep = sep, collapse = collapse)
end
function paste1(items; sep = " ", collapse = nothing)
    if length(items) == 0
        return collapse == nothing ? [] : ""
    end
    collections = []
    max_size = 0
    max_index = 1
    for item in items
        c = []
        if typeof(item) == String
            c = [item]
        else
            for x in item
                push!(c, x)
            end
        end
        push!(collections, c)
        if length(c) > max_size
            max_size = length(c)
            max_index = length(collections)
        end
    end
    for i = 1:length(collections)
        if i != max_index
            collections[i] = cycle(collections[i])
        end
    end
    elements = [join(e, sep) for e in zip(collections...)]
    return collapse == nothing ? elements : join(elements, collapse)
end

"""
Implementation of R function 'match'.
"""
function matchfirst(array1::Array{<:Any}, array2::Array{<:Any})::Array{Integer}
    result = zeros(Integer, length(array1))
    for i = 1:length(array1)
        index = findfirst(isequal(array1[i]), array2)
        if index != nothing
            result[i] = index
        end
    end
    return result
end

"""
    which_first(array)

Returns an array of indices of the index of the first occurance of an element in `array`.
Similar to `which(!duplicated(array))` in R
"""
which_first(array::Vector)::Vector{Int} =
    [findfirst(x -> x == e, array) for e in unique(array)]

"""
    get_element(array, index; default=missing)

Gets the element of `array` at index `index`. Returns `default` if the index is out of range.
"""
function get_element(array::Array{<:Any}, index::Integer; default = missing)
    return 1 <= index <= length(array) ? array[index] : default
end

"""
    set_values!(dictionary, keys, value)

Sets the values of all `keys` in `dictionary` to `value`.
"""
function set_values!(dict::Dict, keys::AbstractArray, value)
    for key in keys
        dict[key] = value
    end
end

"""
    factor(array)

Returns a tuple with an array of the unique array elements and an array of indices.
The second array has same length as the original array. The index indicates the unique element.
That is `getindex(result[1], result[2])` gives the original array.

This implements the functionality of the R function `factor`.
"""
function factor(array::Vector{T})::Tuple{Vector{T},Vector{Int}} where {T}
    levels = unique(array)
    indices_by_levels = Dict(levels[i] => i for i = 1:length(levels))
    indices = [indices_by_levels[array[i]] for i = 1:length(array)]
    return levels, indices
end

function interpolate(input::Input{<:Real}, time::Real)::Real
    return interpolate(input.value, time)
end

function interpolate(value::Real, time::Real)::Real
    return value
end

function interpolate(table::AbstractArray{<:Real,2}, time::Real)::Real
    return interpolate(table[:, 1], table[:, 2], time)
end

function interpolate(
    xvalues::AbstractArray{<:Real},
    yvalues::AbstractArray{<:Real},
    x::Real,
)::Real
    i = searchsortedlast(xvalues, x)
    if i == 0
        return yvalues[1]
    end
    if i == length(yvalues)
        return yvalues[end]
    end
    slope = (yvalues[i+1] - yvalues[i]) / (xvalues[i+1] - xvalues[i])
    return yvalues[i] + slope * (x - xvalues[i])
end

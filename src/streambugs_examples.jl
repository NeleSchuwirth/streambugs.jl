
"""
    streambugs_example_model_toy(;n_reaches, n_habitats)

Create a toy model. The model is ready to run with `run_streambugs`.

- `n_reaches` number of reaches (Default: 3).
- `n_habitats` number of habitats (Default: 2).

"""
function streambugs_example_model_toy(; n_reaches = 3, n_habitats = 2)::Model
    return streambugs_example_model_toy(
        Dict{String,Float64}(),
        n_reaches = n_reaches,
        n_habitats = n_habitats,
    )
end

"""
    streambugs_example_model_toy(default_parameters ;n_reaches, n_habitats)

Create a toy model with `default_parameters`. The model is ready to run with `run_streambugs`.

- `n_reaches` number of reaches (Default: 3). Has to be >= 0.
- `n_habitats` number of habitats (Default: 2).

"""
function streambugs_example_model_toy(
    par::Dict{String,T};
    n_reaches = 3,
    n_habitats = 2,
)::Model{T} where {T<:Real}
    n_cycle_reaches = 3 # every n.cycle.Reaches has same parameters
    n_cycle_habitats = 2 # every n.cycle.Habitats has same parameters
    n_pom = 2
    n_algae = 2
    n_invertebrates = 5

    reaches = paste("Reach", 1:n_reaches, sep = "")
    habitats = paste("Hab", 1:n_habitats, sep = "")
    pom = paste("POM", 1:n_pom, sep = "")
    algae = paste("Alga", 1:n_algae, sep = "")
    invertebrates = paste("Invert", 1:n_invertebrates, sep = "")
    y_names = construct_statevariables(
        reaches,
        habitats,
        pom = pom,
        algae = algae,
        invertebrates = invertebrates,
    )
    y_names = decode_statevarnames(y_names)

    function addifneeded!(par, key, value)
        if haskey(par, key) == false
            par[key] = value
        end
    end

    function set_cycle_param(par, prefix, name, from, to, by, value)
        for i = from:by:to
            addifneeded!(par, prefix * string(i) * "_" * name, value)
        end
    end

    # set-up parameters:
    #    par = Dict{String, Float64}()

    # environmental parameters:
    addifneeded!(par, "w", 10.0)            # m
    # every `n_cycle_reaches` reach is more narrow, starting at Reach1
    set_cycle_param(par, "Reach", "w", 1, n_reaches, n_cycle_reaches, 5.0) # m
    addifneeded!(par, "L", 1000)          # m
    addifneeded!(par, "T", 273.15 + 20)   # K
    addifneeded!(par, "I0", 125)           # W/m2
    addifneeded!(par, "fshade", 0.2)           # -
    # every `n_cycle_habitats` habitat has 20% sufrace of the water shaded
    set_cycle_param(par, "Hab", "fshade", 1, n_habitats, n_cycle_habitats, 0.2) # -
    addifneeded!(par, "CP", 0.02)          # gP/m3
    addifneeded!(par, "CN", 1)             # gN/m3
    addifneeded!(par, "tau", 0.5)           # kg /(s2.m)
    addifneeded!(par, "taucrit", 1)             # kg /(s2.m)

    # initial conditions:
    addifneeded!(par, "POM_Dini", 100)           # gDM/m2
    addifneeded!(par, "Algae_Dini", 100)           # gDM/m2
    addifneeded!(par, "Invertebrates_Dini", 10)            # gDM/m2

    # basal metabolism and energy content parameters:
    addifneeded!(par, "POM_EC", 18000)         # J/gDM
    addifneeded!(par, "Algae_M", 1e-7)          # g
    addifneeded!(par, "Algae_Ea", 0.231)         # eV
    addifneeded!(par, "Algae_b", 0.93)          # -
    addifneeded!(par, "Algae_i0", 4150455552)    # J/a
    addifneeded!(par, "Algae_EC", 20000)         # J/gDM
    addifneeded!(par, "Invertebrates_M", 0.001)         # g
    addifneeded!(par, "Invertebrates_Ea", 0.68642)       # eV
    addifneeded!(par, "Invertebrates_b", 0.695071)      # -
    addifneeded!(par, "Invertebrates_i0", 1.10E+16)      # J/a
    addifneeded!(par, "Invertebrates_EC", 22000)         # J/gDM
    addifneeded!(par, "Algae_cdet", 2)             # s4m2/(kg2a)
    addifneeded!(par, "POM_cdet", 3)             # s4m2/(kg2a)
    addifneeded!(par, "Invertebrates_cdet", 1)             # s4m2/(kg2a)

    # generate stoichiometry of mineralization, respitation, death and production:

    # POM:
    taxa_POM = unique(y_names["y_taxa"][y_names["y_groups"].=="POM"])
    # mineralization:
    set_values!(par, paste("Miner", taxa_POM, taxa_POM, sep = "_"), -1)
    # drift:
    set_values!(par, paste("Drift", taxa_POM, taxa_POM, sep = "_"), -1)

    # Algae:
    taxa_Algae = unique(y_names["y_taxa"][y_names["y_groups"].=="Algae"])
    # respiration:
    set_values!(par, paste("Resp", taxa_Algae, taxa_Algae, sep = "_"), -1)
    # death:
    set_values!(par, paste("Death", taxa_Algae, taxa_Algae, sep = "_"), -1)
    set_values!(par, paste("Death", taxa_Algae, "POM1", sep = "_"), 1) # scale: N/A
    # production:
    set_values!(par, paste("Prod", taxa_Algae, taxa_Algae, sep = "_"), 1)
    # drift:
    set_values!(par, paste("Drift", taxa_Algae, taxa_Algae, sep = "_"), -1)

    # Invertebrates:
    taxa_Invertebrates = unique(y_names["y_taxa"][y_names["y_groups"].=="Invertebrates"])
    # respiration:
    set_values!(par, paste("Resp", taxa_Invertebrates, taxa_Invertebrates, sep = "_"), -1)
    # death:
    set_values!(par, paste("Death", taxa_Invertebrates, taxa_Invertebrates, sep = "_"), -1)
    set_values!(par, paste("Death", taxa_Invertebrates, "POM1", sep = "_"), 1) # scale: N/Aend
    # generate stoichiometry of consumption:
    set_values!(
        par,
        paste("Cons", taxa_Invertebrates, "POM1", taxa_Invertebrates, sep = "_"),
        1,
    ) # scale: N/A
    set_values!(par, paste("Cons", taxa_Invertebrates, "POM1", "POM1", sep = "_"), -1) # scale: N/A
    set_values!(
        par,
        paste("Cons", taxa_Invertebrates, "Alga1", taxa_Invertebrates, sep = "_"),
        1,
    )
    set_values!(par, paste("Cons", taxa_Invertebrates, "Alga1", "Alga1", sep = "_"), -1)
    # scale: making more interactions, invertebrates can cons all Alga or even other
    #        invertebrates
    # scale: check if and when the steady-state is reached (preferably run until all
    #        case-studies reached steady-state) because this may affect performance
    #        if SS is reached fast scale: interactions

    # generate stoichiometry of fish predation:
    set_values!(par, paste("FishPred", "Fish", "Invert1", "Invert1", sep = "_"), -1)
    set_values!(par, paste("FishPred", "Fish", "Invert5", "Invert5", sep = "_"), -1)
    # scale: extra degradation for selected invertebrates; can make more or less of
    #        them

    # drift:
    set_values!(par, paste("Drift", taxa_Invertebrates, taxa_Invertebrates, sep = "_"), -1)

    # process parameters:
    addifneeded!(par, "POM_kminer", 20)            # 1/a

    addifneeded!(par, "Algae_hdens", 1000)          # gDM/m2
    addifneeded!(par, "Algae_KI", 62.1)          # W/m2
    addifneeded!(par, "Algae_KP", 0.01)          # gP/m3
    addifneeded!(par, "Algae_KN", 0.1)           # gN/m3
    addifneeded!(par, "Algae_fresp", 1)             # -
    addifneeded!(par, "Algae_fdeath", 0.7)           # -
    addifneeded!(par, "Algae_fprod", 5)             # -

    addifneeded!(par, "Invertebrates_hdens", 10)            # gDM/m2
    addifneeded!(par, "Invertebrates_fresp", 2.5)           # -
    addifneeded!(par, "Invertebrates_fdeath", 0.7)           # -
    addifneeded!(par, "Invertebrates_fcons", 5)             # -
    addifneeded!(par, "Invertebrates_Kfood", 1)             # gDM/m2
    addifneeded!(par, "Invertebrates_q", 2)
    addifneeded!(par, "Invertebrates_Pref", 1)             # -

    addifneeded!(par, "DFish", 100)           # kg/ha
    # every `n_cycle_reaches` reach has higher fish density, starting at Reach2
    set_cycle_param(par, "Reach", "DFish", 2, n_reaches, n_cycle_reaches, 200) # kg/ha
    addifneeded!(par, "Fish_cfish", 10)            # gDM/kg/d
    addifneeded!(par, "Fish_Kfood", 1)             # gDM/m2
    addifneeded!(par, "Fish_q", 1)             # -
    addifneeded!(par, "Fish_Pref", 1)             # -

    # set-up inputs:
    # --------------
    inpdict = Dict{String,Union{T,AbstractArray{T,2}}}()
    # every `n.cycle.Reaches` reach becomes more narrow at t=1 (from 10m to 2m),
    # starting at Reach3
    if n_reaches >= 3
        set_cycle_param(
            inpdict,
            "Reach",
            "w",
            3,
            n_reaches,
            n_cycle_reaches,
            reshape(convert(Vector{T}, [0, 1, 10, 2]), 2, :),
        )
    end
    inp = [Input(key, value) for (key, value) in inpdict]

    return Model("Toy example", y_names, par, inp)
end

"""
    streambugs_example_model_extended()

Create an extended example model based on the taxa file `test/extended_example-vars_sourcepooltaxa.tsv` and the parameter file `test/extended_example-pars.tsv`. The model is ready to run with `run_streambugs`.
"""
function streambugs_example_model_extended()::Model
    streambugs_example_model_extended(
        Dict{String,Float64}(),
        "test/extended_example-vars_sourcepooltaxa.tsv",
        "test/extended_example-pars.tsv",
    )
end

"""
    streambugs_example_model_extended(default_parameters)

Create an extended example model with `default_parameters` based on the taxa file `test/extended_example-vars_sourcepooltaxa.tsv` and the parameter file `test/extended_example-pars.tsv`. Values in the parameter file will be ignored if defined in `default_parameters`. The model is ready to run with `run_streambugs`.
"""
function streambugs_example_model_extended(par::Dict{String,T})::Model{T} where {T<:Real}
    streambugs_example_model_extended(
        par,
        "test/extended_example-vars_sourcepooltaxa.tsv",
        "test/extended_example-pars.tsv",
    )
end

"""
    streambugs_example_model_extended(default_parameters, taxa_file, parameters_file)

Create an extended example model with `default_parameters` for `taxa_file` and `parameters_file`. Values in the parameter file will be ignored if defined in `default_parameters`. The `taxa_file` is a list of taxa names. The `parameters_file` is a list of key-value pairs separated by TAB characters. The model is ready to run with `run_streambugs`.
"""
function streambugs_example_model_extended(
    par::Dict{String,T},
    taxa_file::String,
    parameters_file::String,
)::Model{T} where {T<:Real}
    invertebrates = readlines(taxa_file)
    parameters = Streambugs.load_model_parameters(parameters_file, default_parameters = par)
    return Model(
        "Extended example",
        reaches = ["108", "174", "109", "172", "173", "442"],
        habitats = ["hab1"],
        pom = ["FPOM", "CPOM"],
        algae = ["crustyAlgae", "filamentousAlgae"],
        invertebrates = invertebrates,
        parameters = parameters,
        inputs = Input{T}[],
    )
end

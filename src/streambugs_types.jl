Maybe{T} = Union{Missing,T}

"""
An Input type is a named value or named interpolation table.

# Examples:
```julia-repl
julia> Input("e", 2.71828)
Input{Float64}("e", 2.71828)
julia> Input("T", [0.1 1.3; 0.3 1.1])
Input{Float64}("T", [0.1 1.3; 0.3 1.1])
```
"""
struct Input{T<:Real}
    name::String
    value::Union{T,AbstractArray{T,2}}
end

"""
    The Streambugs Model with variable names, parameters and (time dependent) inputs
"""
struct Model{T<:Real}
    name::String
    y_names::Dict{String,<:Any}
    parameters::Dict{String,T}
    inputs::Vector{Input{T}}
end

struct ValueAndInputIndex{T<:Real}
    value::T
    input_index::Int
end
struct ValuesAndInputIndices{T<:Real}
    values::Vector{T}
    input_indices::Vector{Tuple{Int,Int}}
end

function Base.:(==)(
    vii1::ValuesAndInputIndices{T},
    vii2::ValuesAndInputIndices{T},
) where {T<:Real}
    return vii1.values == vii2.values && vii1.input_indices == vii2.input_indices
end

struct StateVariableAndCoeff{T<:Real}
    state_variable_index::Int
    coeff::T
end

struct StoichInfo{T<:Real}
    name::String
    Pref::Maybe{ValueAndInputIndex{T}}
    cdet::Maybe{ValueAndInputIndex{T}}
    kminer::Maybe{ValueAndInputIndex{T}}
    fresp::Maybe{ValueAndInputIndex{T}}
    fdeath::Maybe{ValueAndInputIndex{T}}
    fgrotax::Maybe{ValueAndInputIndex{T}}
    KI::Maybe{ValueAndInputIndex{T}}
    KP::Maybe{ValueAndInputIndex{T}}
    KN::Maybe{ValueAndInputIndex{T}}
    hdens::Maybe{ValueAndInputIndex{T}}
    fprod::Maybe{ValueAndInputIndex{T}}
    state_variables::Vector{StateVariableAndCoeff{T}}
    food_index::Int
end

function StoichInfo(
    name::String,
    proc_values_and_input_indices::Dict{String,ValueAndInputIndex{T}},
    state_variables::Vector{StateVariableAndCoeff{T}},
    food_index::Int,
) where {T<:Real}
    StoichInfo(
        name,
        get(proc_values_and_input_indices, "Pref", missing),
        get(proc_values_and_input_indices, "cdet", missing),
        get(proc_values_and_input_indices, "kminer", missing),
        get(proc_values_and_input_indices, "fresp", missing),
        get(proc_values_and_input_indices, "fdeath", missing),
        get(proc_values_and_input_indices, "fgrotax", missing),
        get(proc_values_and_input_indices, "KI", missing),
        get(proc_values_and_input_indices, "KP", missing),
        get(proc_values_and_input_indices, "KN", missing),
        get(proc_values_and_input_indices, "hdens", missing),
        get(proc_values_and_input_indices, "fprod", missing),
        state_variables,
        food_index,
    )
end

struct ProcWebInfo{T<:Real}
    Kfood::Maybe{ValueAndInputIndex{T}}
    cfish::Maybe{ValueAndInputIndex{T}}
    fcons::Maybe{ValueAndInputIndex{T}}
    fgrotax::Maybe{ValueAndInputIndex{T}}
    hdens::Maybe{ValueAndInputIndex{T}}
    q::Maybe{ValueAndInputIndex{T}}
    stoich_infos::Vector{StoichInfo{T}}
end

function ProcWebInfo(
    proc_values_and_input_indices_taxon1::Dict{String,ValueAndInputIndex{T}},
    stoich_infos::Vector{StoichInfo{T}},
) where {T<:Real}
    ProcWebInfo(
        get(proc_values_and_input_indices_taxon1, "Kfood", missing),
        get(proc_values_and_input_indices_taxon1, "cfish", missing),
        get(proc_values_and_input_indices_taxon1, "fcons", missing),
        get(proc_values_and_input_indices_taxon1, "fgrotax", missing),
        get(proc_values_and_input_indices_taxon1, "hdens", missing),
        get(proc_values_and_input_indices_taxon1, "q", missing),
        stoich_infos,
    )
end

struct TraitCondition{T<:Real}
    fcurrent::Vector{T}
    fmicrohab::Vector{T}
    forgmicropoll::Vector{T}
    fsapro::Vector{T}
    ftempmax::Vector{T}
end

struct SystemDefinition{T<:Real}
    y_names::Dict{String,<:Any}
    parameters::Dict{String,T}
    inputs::Vector{Input{T}}
    par_global::Dict{String,ValueAndInputIndex{T}}
    par_env::Dict{String,ValuesAndInputIndices{T}}
    par_habitat_group::Dict{String,Dict{String,ValuesAndInputIndices{T}}}
    par_taxon_group_reach_habitat::Dict{String,ValuesAndInputIndices{T}}
    par_taxon_group::Dict{String,ValuesAndInputIndices{T}}
    stoich_taxon::Dict{String,Dict{String,Dict{String,T}}}
    par_proc_taxon::Vector{Vector{StoichInfo{T}}}
    par_proc_web_cons::Vector{Maybe{ProcWebInfo{T}}}
    par_proc_web_fishpred::Vector{Maybe{ProcWebInfo{T}}}
    max_stoich_infos_length::Int
    par_trait_cond::TraitCondition{T}
end

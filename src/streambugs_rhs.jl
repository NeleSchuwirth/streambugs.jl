# Calculate the right-hand side of the streambugs model
function streambugs_rhs(
    dydt::Vector{FT},
    y_original::Vector{FT},
    sysdef::SystemDefinition{FT},
    time::Real,
) where {FT<:Real}
    y = map(abs, y_original)
    y_names = sysdef.y_names
    y_names_names = y_names["y_names"]
    inputs = sysdef.inputs
    par_global = sysdef.par_global
    par_env = sysdef.par_env
    par_habitat_group = sysdef.par_habitat_group
    par_taxon_group = sysdef.par_taxon_group
    par_taxon_group_reach_habitat = sysdef.par_taxon_group_reach_habitat
    par_proc_taxon = sysdef.par_proc_taxon
    par_proc_web_cons = sysdef.par_proc_web_cons
    par_proc_web_fishpred = sysdef.par_proc_web_fishpred
    par_trait_cond = sysdef.par_trait_cond
    y_food_pref = zeros(FT, sysdef.max_stoich_infos_length)

    i0 = get_values(inputs, par_taxon_group["i0"], time)
    fbaseltax = get_values(inputs, par_taxon_group["fbasaltax"], time)
    M = get_values(inputs, par_taxon_group["M"], time)
    b = get_values(inputs, par_taxon_group["b"], time)
    M0 = get_value(inputs, par_global["M0"], time)
    Ea = get_values(inputs, par_taxon_group["Ea"], time)
    kBoltzmann = get_value(inputs, par_global["kBoltzmann"], time)
    T = get_values(inputs, par_env["T"], time)
    tau = get_values(inputs, par_env["tau"], time)
    taucrit = get_values(inputs, par_env["taucrit"], time)
    EC = get_values(inputs, par_taxon_group["EC"], time)
    fA = get_values(inputs, par_env["fA"], time)
    w = get_values(inputs, par_env["w"], time)
    I0 = get_values(inputs, par_env["I0"], time)
    CP = get_values(inputs, par_env["CP"], time)
    CN = get_values(inputs, par_env["CN"], time)
    fshade = get_values(inputs, par_env["fshade"], time)
    DSusPOM = get_values(inputs, par_env["DSusPOM"], time)
    DFish = get_values(inputs, par_env["DFish"], time)
    input = get_values(inputs, par_taxon_group_reach_habitat["Input"], time)

    r_basal =
        i0 .* fbaseltax .* M .^ (b .- 1) ./ (M0 .^ b) .* exp.(-Ea ./ (kBoltzmann .* T)) ./
        EC .* abs.(y) ./ (fA .* w)
    w_times_fA = w .* fA
    DSusPOM_times_w_times_fA = DSusPOM .* w_times_fA
    copy!(dydt, input .* w_times_fA)

    function warning(type, index, par_name)
        @warn "Stoichiometry defines $type for component $i but parameter '$par_name' " *
              "is not defined for component $i of state vector."
    end

    for i = 1:length(y)
        y_name = y_names_names[i]
        taxon = par_proc_taxon[i]
        cons = par_proc_web_cons[i]
        fish_pred = par_proc_web_fishpred[i]
        for taxa in taxon
            # drift induced by floods:
            # ------------------------
            if taxa.name == "Drift"
                if isnan(tau[i])
                    warning("drift", i, "tau")
                end
                if isnan(taucrit[i])
                    warning("drift", i, "taucrit")
                end
                if isnan(tau[i]) == false &&
                   isnan(taucrit[i]) == false &&
                   tau[i] > taucrit[i]
                    cdet = get_value(inputs, taxa.cdet, time)
                    update(dydt, taxa, cdet * y[i] * (tau[i] - taucrit[i])^2)
                end
            end
            # mineralization:
            # ---------------
            if taxa.name == "Miner"
                kminer = get_value(inputs, taxa.kminer, time)
                update(dydt, taxa, kminer * y[i])
            end
            # respiration:
            # ------------
            if taxa.name == "Resp"
                fresp = get_value(inputs, taxa.fresp, time)
                update(dydt, taxa, fresp * r_basal[i] * w_times_fA[i])
            end
            # death:
            # ------
            if taxa.name == "Death"
                fsapro = par_trait_cond.fsapro[i]
                forgmicropoll = par_trait_cond.forgmicropoll[i]
                fdeath = get_value(inputs, taxa.fdeath, time)
                update(
                    dydt,
                    taxa,
                    fdeath * r_basal[i] * fsapro * forgmicropoll * w_times_fA[i],
                )
            end
            # primary production:
            # -------------------
            if taxa.name == "Prod"
                if isnan(I0[i])
                    warning("Production", i, "I0")
                end
                if isnan(CP[i])
                    warning("Production", i, "CP")
                end
                if isnan(CN[i])
                    warning("Production", i, "CN")
                end
                if isnan(fshade[i])
                    warning("Production", i, "fshade")
                end
                fgrotax = get_value(inputs, taxa.fgrotax, time)
                KI = get_value(inputs, taxa.KI, time)
                KP = get_value(inputs, taxa.KP, time)
                KN = get_value(inputs, taxa.KN, time)
                hdens = get_value(inputs, taxa.hdens, time)
                fprod = get_value(inputs, taxa.fprod, time)

                flimI = I0[i] / (KI + I0[i])
                flimP = CP[i] / (KP + CP[i])
                flimN = CN[i] / (KN + CN[i])
                flimnutrients = min(flimP, flimN)
                fselfshade = hdens / (hdens + y[i] / w_times_fA[i])
                r_prod =
                    fprod *
                    fgrotax *
                    flimI *
                    flimnutrients *
                    fselfshade *
                    (1 - fshade[i]) *
                    r_basal[i]
                update(dydt, taxa, r_prod * w_times_fA[i])
            end
        end
        # consumption:
        # ------------
        if cons !== missing
            fcurrent = par_trait_cond.fcurrent[i]
            ftempmax = par_trait_cond.ftempmax[i]
            fmicrohab = par_trait_cond.fmicrohab[i]
            hdens = get_value(inputs, cons.hdens, time)
            Kdens = hdens * fcurrent * ftempmax * fmicrohab
            fgrotax = get_value(inputs, cons.fgrotax, time)
            Kfood = get_value(inputs, cons.Kfood, time)
            q = get_value(inputs, cons.q, time)
            fcons = get_value(inputs, cons.fcons, time)
            stoich_infos = cons.stoich_infos
            sum_food = calculate_food_infos(
                stoich_infos,
                y,
                inputs,
                y_food_pref,
                time,
                DSusPOM_times_w_times_fA[i],
            )
            fselfinh = Kdens / (Kdens + y[i] / w_times_fA[i])
            sfwq = (sum_food / w_times_fA[i])^q
            Kfq = Kfood^q
            ffoodlim = sum_food >= 0 ? sfwq / (Kfq + sfwq) : sfwq / Kfq
            r_cons_tot = fcons * fgrotax * fselfinh * ffoodlim * r_basal[i]
            for si = 1:length(stoich_infos)
                stoich_info = stoich_infos[si]
                update(dydt, stoich_info, r_cons_tot * y_food_pref[si] * w_times_fA[i])
            end
        end
        # fish predation:
        # ---------------
        if fish_pred !== missing
            cfish = get_value(inputs, fish_pred.cfish, time)
            Kfood = get_value(inputs, fish_pred.Kfood, time)
            q = get_value(inputs, fish_pred.q, time)
            stoich_infos = fish_pred.stoich_infos
            sum_food = calculate_food_infos(
                stoich_infos,
                y,
                inputs,
                y_food_pref,
                time,
                DSusPOM_times_w_times_fA[i],
            )
            sfwq = (sum_food / w_times_fA[i])^q
            ffoodlim = sfwq / (Kfood^q + sfwq)
            r_fishpred_tot = DFish[i] / 10000 * cfish * 365.25 * ffoodlim
            for si = 1:length(stoich_infos)
                stoich_info = stoich_infos[si]
                update(dydt, stoich_info, r_fishpred_tot * y_food_pref[si] * w_times_fA[i])
            end
        end
    end
    for i = 1:length(dydt)
        if y_original[i] < zero(FT)
            dydt[i] = -5.0 * y_original[i]
        end
        if isnan(y_original[i])
            dydt[i] = 1e-20
        end
    end
end

function calculate_food_infos(
    stoich_infos::Vector{StoichInfo{T}},
    y::Vector{T},
    inputs::Vector{Input{T}},
    y_food_pref::Vector{T},
    time::Real,
    default_y_food::T,
)::T where {T<:Real}
    sum_food = zero(T)
    sum_food_pref = zero(T)
    for i = 1:length(stoich_infos)
        stoich_info = stoich_infos[i]
        food_index = stoich_info.food_index
        y_food = food_index == 0 ? default_y_food : y[food_index]
        pref = get_value(inputs, stoich_info.Pref, time)
        sum_food += y_food
        sum_food_pref += y_food * pref
        y_food_pref[i] = y_food * pref
    end
    for i = 1:length(stoich_infos)
        y_food_pref[i] = sum_food_pref > zero(T) ? y_food_pref[i] / sum_food_pref : zero(T)
    end
    return sum_food
end

function update(dydt, stoich_info::StoichInfo{T}, rate::T) where {T<:Real}
    for state_variable in stoich_info.state_variables
        dydt[state_variable.state_variable_index] += state_variable.coeff * rate
    end
end

function get_values(
    inputs::Vector{Input{T}},
    values_and_indices::ValuesAndInputIndices{T},
    time::Real,
)::Vector{T} where {T<:Real}
    if length(values_and_indices.input_indices) == 0
        return values_and_indices.values
    end
    values = copy(values_and_indices.values)
    for (i, input_index) in values_and_indices.input_indices
        values[i] = interpolate(inputs[input_index], time)
    end
    return values
end

function get_value(
    inputs::Vector{Input{T}},
    value_and_index::ValueAndInputIndex{T},
    time::Real,
)::T where {T<:Real}
    if value_and_index.input_index == 0
        return value_and_index.value
    end
    return interpolate(inputs[value_and_index.input_index], time)
end

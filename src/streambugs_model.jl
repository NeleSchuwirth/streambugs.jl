"""
    Model(name; reaches, habitats, pom, algae, invertebrates, parameters, inputs)

Define the streambugs model.

- `name`: The name of the model.
- `reaches`: Vector of reach names (Default: empty vector).
- `habitats`: Vector of habitat names (Default: empty vector).
- `pom`: Vector of names of POM (particulate organic matter) group (Default: empty vector).
- `algae`: Vector of names of algae group (Default: empty vector).
- `invertebrates`: Vector of names of invertebrates group (Default: empty vector).
- `parameters`: Parameters as a dictionary (Default: empty dictionary).
- `inputs`: Vector of all (time dependent) inputs (Default: empty vector).

# Example:
```julia-repl
julia> invertebrates = load_invertebrates("input/sourcepooltaxa_extended_example.dat")
julia> pom = ["FPOM", "CPOM"]
julia> algae = ["crustyAlgae", "filamentousAlgae"]
julia> reaches = ["108", "174", "109", "172", "173", "442"]
julia> habitats = ["hab1"]
julia> parameters = load_parameters("input/pars_extended_example.dat")
julia> model = Model("model"; reaches, habitats, pom, algae, invertebrates, parameters)

```
"""
Model(
    name::String;
    reaches::Vector{String} = String[],
    habitats::Vector{String} = String[],
    pom::Vector{String} = String[],
    algae::Vector{String} = String[],
    invertebrates::Vector{String} = String[],
    parameters::Dict{String,T} = Dict{String,Float64}(),
    inputs::Vector{Input{T}} = Input{Float64}[],
) where {T<:Real} = begin
    y_names = Streambugs.decode_statevarnames(
        Streambugs.construct_statevariables(
            reaches,
            habitats,
            pom = pom,
            algae = algae,
            invertebrates = invertebrates,
        ),
    )
    Streambugs.Model(name, y_names, parameters, inputs)
end

"""
    get_parameter_names_values(model, name_filter)

Get from the `model` a tuple with parameter names and values which can be used for the keyword parameters `parameter_names` and `parameter_values` in `run_streambugs`.

# Example:
```julia-repl
julia> get_parameter_names_values(streambugs_example_model_toy(), name_filter=pn->endswith(pn,r"Kfood"))
(["Fish_Kfood", "Invertebrates_Kfood"], [1.0, 1.0])
```
"""
function get_parameter_names_values(
    model::Model{T};
    name_filter::Function = name -> true,
)::Tuple{Vector{String},Vector{T}} where {T<:Real}
    par_pairs = sort(collect(filter(kv -> name_filter(kv.first), model.parameters)))
    return first.(par_pairs), last.(par_pairs)
end

"""
    load_model_parameters(parameters_file; default_parameters)

Load parameters from `parameters_file` which can be used for the keyword parameter `parameters` of `Model` constructor. The file should contain key-value pairs separated by a TAB character. The read value is ignored if for the same key a value is found in dictionary `default_parameters`.

# Example:
```julia-repl
julia> load_model_parameters("test/extended_example-pars.tsv"; default_parameters=Dict("Simulium_feedingtype_min"=>0.23))
Dict{String, Float64} with 2526 entries:
  "Simulium_feedingtype_min"                                    => 0.23
  "Cons_Helobdellastagnalis_Habroleptoides_Habroleptoides"      => -1.25
  "Asellusaquaticus_tempmaxtolval_class1"                       => 1.0
  "Cons_Limnephilidae_CPOM_FPOM"                                => 1.83333
  "Polycentropodidae_microhabtolval_type7"                      => 0.39
  "Cons_Helobdellastagnalis_Limnius_Limnius"                    => -1.25
  "Cons_Erpobdellaoctoculata_Asellusaquaticus_Asellusaquaticus" => -1.25
  "Cons_Tabanidae_FPOM_FPOM"                                    => -1.22222
  "Cons_Gammarusfossarum_filamentousAlgae_filamentousAlgae"     => -1.20879
  "Paraleptophlebia_feedingtype_xyl"                            => 0.0
  "Gyraulus_orgmicropolltolval_class1"                          => 1.0
  ⋮                                                             => ⋮

```
"""
function load_model_parameters(
    parameters_file::String;
    default_parameters::Dict{String,T} = Dict{String,Float64}(),
)::Dict{String,T} where {T<:Real}
    parameters = Dict{String,T}()
    for line in readlines(parameters_file)
        row = split(line, "\t")
        parname = row[1]
        parameters[parname] =
            haskey(default_parameters, parname) ? default_parameters[parname] :
            parse(Float64, row[2])
    end
    return parameters
end

"""
    get_y_ini(model::Model)

Calculate an initial state from the `model`.

# Example:
```julia-repl
julia> get_y_ini(streambugs_example_model_toy())
54-element Vector{Float64}:
 250.0
 250.0
 250.0
 250.0
  25.0
  25.0
   ⋮
  50.0
  50.0
  50.0
  50.0
  50.0
```
"""
function get_y_ini(model::Model)::Vector{<:Real}
    sys_def = Streambugs.SystemDefinition(model)
    inputs = sys_def.inputs
    y_ini =
        Streambugs.get_values(inputs, sys_def.par_taxon_group_reach_habitat["Dini"], 0.0) .*
        Streambugs.get_values(inputs, sys_def.par_env["fA"], 0.0) .*
        Streambugs.get_values(inputs, sys_def.par_env["w"], 0.0)
    return y_ini
end

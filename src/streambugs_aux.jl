import Base.string

"""
    construct_statevariables(reaches, habitats; pom, algae, invertebrates)

Construct the streambugs ODE state variable names.

Construct encoded labels of streambugs ODE state variable names from reach
names, habitat names, and "taxa" names for at least one of the POM, Algae, or
Invertebrates groups. A vector with state variable names of the form `Reach_Habitat_Taxon_Group` is returned.

# Arguments
- `reaches::Vector{String}`: reaches names (duplicates are dropped)
- `habitats::Vector{String}`: habitat names (duplicates are dropped)
- `pom`: optional ("taxa") name(s) of POM (particulate organic matter) group (duplicates are dropped)
- `algae`: optional name(s) of Algae group (duplicates are dropped)
- `invertebrates`: optional name(s) of Invertebrates group (duplicates are dropped)

# Example
```julia-repl
julia> reaches = Streambugs.paste("Reach", 1:2; sep="")
2-element Vector{String}:
 "Reach1"
 "Reach2"

julia> habitats = Streambugs.paste("Habitat", 1:2; sep="")
2-element Vector{String}:
 "Habitat1"
 "Habitat2"

julia> construct_statevariables(reaches, habitats; invertebrates=["Baetis","Ecdyonurus"])
8-element Vector{String}:
 "Reach1_Habitat1_Baetis_Invertebrates"
 "Reach1_Habitat1_Ecdyonurus_Invertebrates"
 "Reach1_Habitat2_Baetis_Invertebrates"
 "Reach1_Habitat2_Ecdyonurus_Invertebrates"
 "Reach2_Habitat1_Baetis_Invertebrates"
 "Reach2_Habitat1_Ecdyonurus_Invertebrates"
 "Reach2_Habitat2_Baetis_Invertebrates"
 "Reach2_Habitat2_Ecdyonurus_Invertebrates"
```
"""
function construct_statevariables(
    reaches::Vector{String},
    habitats::Vector{String};
    pom = nothing,
    algae = nothing,
    invertebrates = nothing,
)::Vector{String}
    function is_empty(array)
        return array == nothing || length(array) == 0
    end
    # validate that at least one taxon or POM was provided
    if is_empty(pom) && is_empty(algae) && is_empty(invertebrates)
        throw(
            ArgumentError(
                "Missing at least one \"taxon\" from either group " *
                "of POM, Algae, or Invertebrates",
            ),
        )
    end

    # rm duplicates
    if length(unique(reaches)) != length(reaches)
        @warn "Non unique reaches names - dropping duplicates"
        reaches = unique(reaches)
    end
    if length(unique(habitats)) != length(habitats)
        @warn "Non unique habitats names - dropping duplicates"
        habitats = unique(habitats)
    end

    # merge taxa groups
    pom_enc = is_empty(pom) ? [] : paste(pom, "POM", sep = "_")
    algae_enc = is_empty(algae) ? [] : paste(algae, "Algae", sep = "_")
    invertebrates_enc =
        is_empty(invertebrates) ? [] : paste(invertebrates, "Invertebrates", sep = "_")
    taxa_enc = cat(pom_enc, algae_enc, invertebrates_enc, dims = 1)

    # rm duplicates
    if length(unique(taxa_enc)) != length(taxa_enc)
        @warn "Non unique taxa names within a group - dropping duplicates"
        taxa_enc = unique(taxa_enc)
    end

    # encode state variables: (reach_i, habitat_i) \times taxon_j
    n_reaches = length(reaches)
    n_habitats = length(habitats)
    n_taxa = length(taxa_enc)
    y_names = fill("", n_reaches * n_habitats * n_taxa)
    for i = 1:n_reaches
        for j = 1:n_habitats
            indices = (i - 1) * n_habitats * n_taxa + (j - 1) * n_taxa .+ (1:n_taxa)
            y_names[indices] = paste(reaches[i], habitats[j], taxa_enc, sep = "_")
        end
    end

    # sanity check: all values set
    if any(map(n -> n == "", y_names))
        throw(ArgumentError("problem constructing state variables"))
    end

    return y_names
end

"""
    decode_statevarnames(y_names)

Decode the streambugs ODE state variable names.

Extract reach names, habitat names, taxa names and optional group names from
encoded labels of streambugs ODE state variable names.

# Arguments
- `y_names::Vector{String}`: state variable names in the form of `Reach_Habitat_Taxon` or `Reach_Habitat_Taxon_Group`

# Return value
A dictionary with
- `y_names`: state variable names (input argument)
- `y_reaches`: names of reaches
- `y_habitats`: names of habitats
- `y_taxa`: names of taxa
- `y_groups`: unique names of groups
- `reaches`: unique names of reaches
- `habitats`: unique names of habitats
- `taxa`: unique names of taxa
- `groups`: unique names of groups
- `indfA`: indices used for the areal fractions of each reach and habitat

# Example
```julia-repl
julia> decode_statevarnames(["Reach1_Hab1_CPOM_POM","Reach2_Hab1_FPOM_POM"])
Dict{String, Any} with 10 entries:
  "habitats"   => ["Hab1"]
  "groups"     => ["POM"]
  "indfA"      => Dict("Reach2"=>Dict("ind_reach"=>[2], "ind_1sthab"=>[1]), "Reach1"=>Dict("ind_reach"=>[1], "ind_1sthab"=>[1]))
  "y_reaches"  => ["Reach1", "Reach2"]
  "y_names"    => ["Reach1_Hab1_CPOM_POM", "Reach2_Hab1_FPOM_POM"]
  "y_groups"   => ["POM", "POM"]
  "reaches"    => ["Reach1", "Reach2"]
  "taxa"       => ["CPOM", "FPOM"]
  "y_habitats" => ["Hab1", "Hab1"]
  "y_taxa"     => ["CPOM", "FPOM"]
```
"""
function decode_statevarnames(y_names::Vector{String})
    # split names of state variables:
    y_reaches = fill("", length(y_names))
    y_habitats = fill("", length(y_names))
    y_taxa = fill("", length(y_names))
    y_groups = fill("", length(y_names))
    for i = 1:length(y_names)
        splitted_y_name = split(y_names[i], "_")
        y_reaches[i] = get_element(splitted_y_name, 1, default = "")
        y_habitats[i] = get_element(splitted_y_name, 2, default = "")
        y_taxa[i] = get_element(splitted_y_name, 3, default = "")
        y_groups[i] = get_element(splitted_y_name, 4, default = "")
    end

    # determine reach, habitat, taxon and group names:
    reaches = unique(collect(skipmissing(y_reaches)))
    habitats = unique(collect(skipmissing(y_habitats)))
    taxa = unique(collect(skipmissing(y_taxa)))
    groups = unique(collect(skipmissing(y_groups)))

    # issue warnings:
    reaches_missing = findall(isequal(""), y_reaches)
    if length(reaches_missing) > 0
        @warn "Undefined reach name(s) in component(s) of state vector: " *
              paste(reaches_missing, collapse = ",")
    end
    habitats_missing = findall(isequal(""), y_habitats)
    if length(habitats_missing) > 0
        @warn "Undefined habitat name(s) in component(s) of state vector: " *
              paste(habitats.na, collapse = ",")
    end
    taxa_missing = findall(isequal(""), y_taxa)
    if length(taxa_missing) > 0
        @warn "Undefined taxon name(s) in component(s) of state vector: " *
              paste(taxa_missing, collapse = ",")
    end
    y_groups[findall(isequal(""), y_groups)] .= ""

    indfA = Dict{String,Dict{String,Vector{Int}}}()
    for reach in reaches
        ind_reach = findall(y_reaches .== reach)
        habs_reach = unique(y_habitats[ind_reach])
        ind_1sthab = matchfirst(habs_reach, y_habitats[ind_reach])
        indfA[reach] = Dict("ind_reach" => ind_reach, "ind_1sthab" => ind_1sthab)
    end

    # return splitted state variable names and reach, habitat, taxa and group names:
    return Dict(
        "y_names" => y_names,
        "y_reaches" => y_reaches,
        "y_habitats" => y_habitats,
        "y_taxa" => y_taxa,
        "y_groups" => y_groups,
        "reaches" => reaches,
        "habitats" => habitats,
        "taxa" => taxa,
        "groups" => groups,
        "indfA" => indfA,
    )
end

function get_y_names_indices_by_name_hierarchy(
    y_names::Vector{String},
)::Dict{String,Dict{String,Dict{String,Int}}}
    result = Dict{String,Dict{String,Dict{String,Int}}}()
    for i = 1:length(y_names)
        y_name = y_names[i]
        splitted_name = split(y_name, "_")
        if length(splitted_name) < 3
            throw(ArgumentError("State variable '$(y_name)' has less than 3 parts."))
        end
        reach = splitted_name[1]
        habitat = splitted_name[2]
        taxon = splitted_name[3]
        if haskey(result, reach) == false
            result[reach] = Dict{String,Dict{String,Int}}()
        end
        if haskey(result[reach], habitat) == false
            result[reach][habitat] = Dict{String,Int}()
        end
        if haskey(result[reach][habitat], taxon)
            throw(
                ArgumentError(
                    "State variable '$(y_name)' is invalid because there is " *
                    "already a state variable with same reach, habitat and taxon.",
                ),
            )
        end
        result[reach][habitat][taxon] = i
    end
    return result
end

function convert_to_values_and_input_indices(
    valuesAndInputIndices::Vector{ValueAndInputIndex{T}},
)::ValuesAndInputIndices{T} where {T<:Real}
    values = map(e -> e.value, valuesAndInputIndices)
    input_indices = Tuple{Int,Int}[]
    for i = 1:length(valuesAndInputIndices)
        input_index = valuesAndInputIndices[i].input_index
        if input_index > 0
            push!(input_indices, (i, input_index))
        end
    end
    return ValuesAndInputIndices(values, input_indices)
end

function get_value_and_input_index(
    valuesAndInputIndices::ValuesAndInputIndices{T},
    i::Int,
)::ValueAndInputIndex{T} where {T<:Real}
    value = valuesAndInputIndices.values[i]
    input_index = 0
    for (index, iindex) in valuesAndInputIndices.input_indices
        if index == i
            input_index = iindex
            break
        end
    end
    return ValueAndInputIndex(value, input_index)
end

function create_dummy_values_and_input_indices(n::Int)::ValuesAndInputIndices
    return ValuesAndInputIndices(repeat([NaN, n]), Tuple{Int,Int}[])
end

function get_values_and_indices_global(
    par_names::Vector{String},
    parameters::Dict{String,T},
    inputs::Vector{Input{T}} = Input{T}[],
    defaults::Dict{String,<:Real} = Dict{String,Float64}(),
)::Dict{String,ValueAndInputIndex{T}} where {T<:Real}
    input_indices_by_name = Dict(inputs[i].name => i for i = 1:length(inputs))
    pars = merge(defaults, parameters)
    values_and_indices = Dict{String,ValueAndInputIndex{T}}()
    for par_name in par_names
        value = get(pars, par_name, NaN)
        input_index = get(input_indices_by_name, par_name, 0)
        values_and_indices[par_name] = ValueAndInputIndex(value, input_index)
    end
    return values_and_indices
end

function get_parameter_names(
    name::String,
    parameters::Dict{String,T},
)::Vector{String} where {T<:Real}
    par_names = Set{String}()
    for par_name in keys(parameters)
        splitted_par_name = split(par_name, "_")
        index = findfirst(isequal(name), splitted_par_name)
        if index != nothing && index < length(splitted_par_name)
            push!(par_names, name * "_" * splitted_par_name[index+1])
        end
    end
    return collect(par_names)
end

function get_values_and_indices_global_envcondtraits(
    trait_names::Vector{String},
    parameters::Dict{String,T},
    inputs::Vector{Input{T}} = Input{T}[],
    defaults::Dict{String,T} = Dict{String,T}(),
)::Dict{String,Dict{String,ValueAndInputIndex{T}}} where {T<:Real}
    result = Dict{String,Dict{String,ValueAndInputIndex{T}}}()
    for trait_name in trait_names
        par_names = get_parameter_names(trait_name, parameters)
        values = get_values_and_indices_global(par_names, parameters, inputs, defaults)
        result[trait_name] =
            Dict(string(split(e.first, "_")[end]) => e.second for e in values)
    end
    return result
end

function get_values_and_indices(
    prefix_hierarchies::Vector{Vector{String}},
    y_prefix_indices::Vector{Int},
    par_names::Vector{String},
    parameters::Dict{String,T},
    inputs::Vector{Input{T}} = Input{T}[],
    defaults::Dict{String,<:Real} = Dict{String,Float64}(),
)::Dict{String,ValuesAndInputIndices{T}} where {T<:Real}
    pars = merge(defaults, parameters)
    input_indices_by_name = Dict(inputs[i].name => i for i = 1:length(inputs))
    result = Dict{String,ValuesAndInputIndices{T}}()
    for par_name in Set(par_names)
        values_and_indices =
            Vector{ValueAndInputIndex{T}}(undef, length(prefix_hierarchies))
        for i = 1:length(prefix_hierarchies)
            value = NaN
            index = 0
            for prefix in prefix_hierarchies[i]
                name = prefix * par_name
                if isnan(value) && haskey(pars, name)
                    value = pars[name]
                end
                if index == 0 && haskey(input_indices_by_name, name)
                    index = input_indices_by_name[name]
                end
            end
            values_and_indices[i] = ValueAndInputIndex(convert(T, value), index)
        end
        result[par_name] = convert_to_values_and_input_indices(
            getindex(values_and_indices, y_prefix_indices),
        )
    end
    return result
end

function calculate_combinations(names...)
    joined_names = paste1(names, sep = "_")
    _, prefix_indices = factor(joined_names)
    first_indices = which_first(prefix_indices)
    result = []
    for i in first_indices
        push!(result, getindex.(names, i))
    end
    return prefix_indices, result
end

function get_values_and_indices_reach(
    par_names::Array{String},
    y_names::Dict{String,<:Any},
    parameters::Dict{String,T},
    inputs::Array{Input{T}} = Input{T}[],
    defaults::Dict{String,T} = Dict{String,T}(),
)::Dict{String,ValuesAndInputIndices{T}} where {T<:Real}
    unique_names, y_prefix_indices = factor(y_names["y_reaches"])
    prefix_hierarchies = [[prefix * "_", ""] for prefix in unique_names]
    return get_values_and_indices(
        prefix_hierarchies,
        y_prefix_indices,
        par_names,
        parameters,
        inputs,
        defaults,
    )
end

function get_values_and_indices_habitat(
    par_names::Array{String},
    y_names::Dict{String,<:Any},
    parameters::Dict{String,T},
    inputs::Array{Input{T}} = Input{T}[],
    defaults::Dict{String,<:Real} = Dict{String,Float64}(),
)::Dict{String,ValuesAndInputIndices{T}} where {T<:Real}
    prefix_indices, combinations =
        calculate_combinations(y_names["y_reaches"], y_names["y_habitats"])
    prefix_hierarchies = [[
        e[1] * "_" * e[2] * "_", # Reach_Habitat_Par
        e[2] * "_",              # Habitat_Par
        e[1] * "_",              # Reach_Par
        "",                      # Par
    ] for e in combinations]
    result = get_values_and_indices(
        prefix_hierarchies,
        prefix_indices,
        par_names,
        parameters,
        inputs,
        defaults,
    )
    # fA normalization
    if haskey(result, "fA")
        fA_values_and_input_indices = result["fA"]
        normalized = normalize_fA(fA_values_and_input_indices.values, y_names)
        result["fA"] =
            ValuesAndInputIndices(normalized, fA_values_and_input_indices.input_indices)
    end
    return result
end

function get_values_and_indices_habitat_group(
    group_names::Vector{String},
    y_names::Dict{String,<:Any},
    parameters::Dict{String,T},
    inputs::Array{Input{T}} = Input{T}[],
    defaults::Dict{String,<:Real} = Dict{String,Float64}(),
)::Dict{String,Dict{String,ValuesAndInputIndices{T}}} where {T<:Real}
    result = Dict{String,Dict{String,ValuesAndInputIndices{T}}}()
    for group_name in group_names
        par_names = get_parameter_names(group_name, parameters)
        values =
            get_values_and_indices_habitat(par_names, y_names, parameters, inputs, defaults)
        result[group_name] =
            Dict(string(split(e.first, "_")[end]) => e.second for e in values)
    end
    return result
end

function normalize_fA(values::Vector{T}, y_names::Dict{String,<:Any}) where {T<:Real}
    normalized_values = copy(values)
    for fA_indices in Base.values(y_names["indfA"])
        sampled_indices = fA_indices["ind_reach"]
        sampled_values = getindex(values, sampled_indices)
        sampled_values /= sum(getindex(sampled_values, fA_indices["ind_1sthab"]))
        setindex!(normalized_values, sampled_values, sampled_indices)
    end
    return normalized_values
end

function get_values_and_indices_taxon_group(
    par_names::Array{String},
    y_names::Dict{String,<:Any},
    parameters::Dict{String,T},
    inputs::Array{Input{T}} = Input{T}[],
    defaults::Dict{String,<:Real} = Dict{String,Float64}(),
)::Dict{String,ValuesAndInputIndices{T}} where {T<:Real}
    prefix_indices, combinations =
        calculate_combinations(y_names["y_taxa"], y_names["y_groups"])
    prefix_hierarchies = [[
        e[1] * "_", # Taxon_Par
        e[2] * "_", # Group_Par
        "",         # Par
    ] for e in combinations]
    return get_values_and_indices(
        prefix_hierarchies,
        prefix_indices,
        par_names,
        parameters,
        inputs,
        defaults,
    )
end

function get_values_and_indices_taxaprop_traits(
    xval_pars::Dict{String,Dict{String,<:Any}},
    y_names::Dict{String,<:Any},
    parameters::Dict{String,T},
    inputs::Array{Input{T}} = Input{T}[],
    defaults::Dict{String,<:Real} = Dict{String,Float64}(),
)::Dict{String,Dict{String,ValuesAndInputIndices{T}}} where {T<:Real}
    result = Dict{String,Dict{String,ValuesAndInputIndices{T}}}()
    for e in xval_pars
        trait_name = e.first
        par_names = map(n -> trait_name * "_" * n, collect(keys(e.second)))
        values = get_values_and_indices_taxon_group(
            par_names,
            y_names,
            parameters,
            inputs,
            defaults,
        )
        result[trait_name] =
            Dict(string(split(e.first, "_")[end]) => e.second for e in values)
    end
    return result
end

function get_values_and_indices_taxon_group_reach_habitat(
    par_names::Vector{String},
    y_names::Vector{String},
    parameters::Dict{String,T},
    inputs::Vector{Input{T}} = Input{T}[],
    defaults::Dict{String,<:Real} = Dict{String,Float64}(),
)::Dict{String,ValuesAndInputIndices{T}} where {T<:Real}
    hierarchy = [
        ["Taxon", "Reach", "Habitat", "Par"],
        ["Group", "Reach", "Habitat", "Par"],
        ["Taxon", "Habitat", "Par"],
        ["Group", "Habitat", "Par"],
        ["Taxon", "Reach", "Par"],
        ["Group", "Reach", "Par"],
        ["Taxon", "Par"],
        ["Group", "Par"],
        ["Reach", "Habitat", "Par"],
        ["Habitat", "Par"],
        ["Reach", "Par"],
        ["Par"],
    ]
    pars = merge(defaults, parameters)
    input_indices_by_name = Dict(inputs[i].name => i for i = 1:length(inputs))
    result = Dict{String,ValuesAndInputIndices{T}}()
    for par_name in par_names
        values_and_indicies = Vector{ValueAndInputIndex{T}}(undef, length(y_names))
        for i = 1:length(y_names)
            reach, habitat, taxon, group = split(y_names[i], "_")
            bindings = Dict(
                "Reach" => reach,
                "Habitat" => habitat,
                "Taxon" => taxon,
                "Group" => group,
                "Par" => par_name,
            )
            values_and_indicies[i] =
                get_value_and_index(hierarchy, pars, input_indices_by_name, bindings)
        end
        result[par_name] = convert_to_values_and_input_indices(values_and_indicies)
    end
    return result
end

function get_value_and_index(
    hierarchy::Vector{Vector{String}},
    parameters::Dict{String,T},
    input_indices_by_name::Dict{String,Int},
    bindings::Dict{String,<:AbstractString},
)::ValueAndInputIndex{T} where {T<:Real}
    value = NaN
    index = 0
    for prefix in hierarchy
        full_par_name = join([bindings[n] for n in prefix], "_")
        if isnan(value) && haskey(parameters, full_par_name)
            value = parameters[full_par_name]
        end
        if index == 0 && haskey(input_indices_by_name, full_par_name)
            index = input_indices_by_name[full_par_name]
        end
        if isnan(value) == false && index != 0
            break
        end
    end
    return ValueAndInputIndex(convert(T, value), index)
end

function get_stoich_taxon(
    proc::String,
    parameters::Dict{String,T},
)::Dict{String,Dict{String,T}} where {T<:Real}
    result = Dict{String,Dict{String,T}}()
    for par_name in keys(parameters)
        splitted_par_name = split(par_name, "_")
        if splitted_par_name[1] == proc
            if length(splitted_par_name) != 3
                throw(
                    ArgumentError(
                        "Parameter started with '$(proc)' does not have " *
                        "3 components: $(par_name)",
                    ),
                )
            end
            c2 = splitted_par_name[2]
            if haskey(result, c2) == false
                result[c2] = Dict{String,T}()
            end
            result[c2][splitted_par_name[3]] = parameters[par_name]
        end
    end
    return result
end

function get_stoich_web(
    proc::String,
    parameters::Dict{String,T},
)::Dict{String,Dict{String,Dict{String,T}}} where {T<:Real}
    result = Dict{String,Dict{String,Dict{String,T}}}()
    for par_name in keys(parameters)
        splitted_par_name = split(par_name, "_")
        if splitted_par_name[1] == proc
            if length(splitted_par_name) != 4
                throw(
                    ArgumentError(
                        "Parameter started with '$(proc)' does not have " *
                        "4 components: $(par_name)",
                    ),
                )
            end
            c2 = splitted_par_name[2]
            if haskey(result, c2) == false
                result[c2] = Dict{String,Dict{String,T}}()
            end
            c3 = splitted_par_name[3]
            if haskey(result[c2], c3) == false
                result[c2][c3] = Dict{String,T}()
            end
            result[c2][c3][splitted_par_name[4]] = parameters[par_name]
        end
    end
    return result
end

function get_par_proc_taxon(
    kinparnames_taxon::Dict{String,Vector{String}},
    stoich_taxon::Dict{String,Dict{String,Dict{String,T}}},
    y_name_indices::Dict{String,Dict{String,Dict{String,Int}}},
    y_names::Vector{String},
    parameters::Dict{String,T},
    inputs::Array{Input{T}} = Input{T}[],
)::Dict{String,Vector{StoichInfo{T}}} where {T<:Real}
    kinpar_names = reduce(vcat, values(kinparnames_taxon))
    values_and_indices = get_values_and_indices_taxon_group_reach_habitat(
        kinpar_names,
        y_names,
        parameters,
        inputs,
        Dict("fgrotax" => 1.0),
    )
    result = Dict{String,Vector{StoichInfo}}()
    for i = 1:length(y_names)
        y_name = y_names[i]
        info = Vector{StoichInfo{T}}()
        reach, habitat, taxon, group = split(y_name, "_")
        for proc_name in keys(kinparnames_taxon)
            proc_values_and_indices = Dict{String,ValueAndInputIndex{T}}()
            for kinpar_name in kinparnames_taxon[proc_name]
                proc_values_and_indices[kinpar_name] =
                    get_value_and_input_index(values_and_indices[kinpar_name], i)
            end
            proc_par_stoich = stoich_taxon[proc_name]
            stoich_info = StoichInfo(
                proc_name,
                proc_values_and_indices,
                StateVariableAndCoeff{T}[],
                0,
            )
            if haskey(proc_par_stoich, taxon)
                stoich = proc_par_stoich[taxon]
                for related_taxon in sort(collect(keys(stoich)))
                    index = y_name_indices[reach][habitat][related_taxon]
                    coeff = stoich[related_taxon]
                    push!(stoich_info.state_variables, StateVariableAndCoeff(index, coeff))
                end
            end
            if length(stoich_info.state_variables) > 0
                push!(info, stoich_info)
            end
        end
        result[y_name] = info
    end
    return result
end

function get_par_proc_web(
    kinparnames_web::Dict{String,Dict{String,Vector{String}}},
    stoich_web::Dict{String,Dict{String,Dict{String,Dict{String,T}}}},
    y_name_indices::Dict{String,Dict{String,Dict{String,Int}}},
    y_names::Vector{String},
    indfA::Dict{String,Dict{String,Vector{Int}}},
    parameters::Dict{String,T},
    inputs::Array{Input{T}} = Input{T}[],
)::Dict{String,Vector{Dict{String,ProcWebInfo{T}}}} where {T<:Real}
    indices_1sthabinreach = sort(get_indices_1sthabinreach(indfA))
    # add names where taxon part is replaced by "Fish"
    y_names_extended = vcat(
        y_names,
        map(
            e -> join(e, "_"),
            map(
                e -> setindex!(e, "Fish", 3),
                split.(getindex(y_names, indices_1sthabinreach), "_"),
            ),
        ),
    )
    kinpar_names_taxon1 = reduce(vcat, [v["taxon1"] for v in values(kinparnames_web)])
    values_and_indices_taxon1 = get_values_and_indices_taxon_group_reach_habitat(
        kinpar_names_taxon1,
        y_names_extended,
        parameters,
        inputs,
        Dict("fgrotax" => 1.0, "q" => 1.0),
    )
    input_indices_by_name = Dict(inputs[i].name => i for i = 1:length(inputs))
    parameters_taxa2 = merge(Dict("Pref" => 1.0), parameters)
    result = Dict{String,Vector{Dict{String,ProcWebInfo{T}}}}(n => [] for n in y_names)
    for i = 1:length(y_names_extended)
        y_name = y_name_extended = y_names_extended[i]
        if i > length(y_names)
            y_name = y_names[indices_1sthabinreach[i-length(y_names)]]
        end
        reach, habitat, taxon, group = split(y_name_extended, "_")
        proc_infos = Dict{String,ProcWebInfo}()
        for proc_name in keys(kinparnames_web)
            kpnames = kinparnames_web[proc_name]
            proc_values_and_indices_taxon1 = Dict{String,ValueAndInputIndex{T}}(
                name => get_value_and_input_index(values_and_indices_taxon1[name], i)
                for name in kpnames["taxon1"]
            )
            stoich = stoich_web[proc_name]
            if haskey(stoich, taxon)
                info = get_proc_web_info(
                    y_name_indices,
                    y_names,
                    proc_values_and_indices_taxon1,
                    kpnames,
                    stoich[taxon],
                    parameters_taxa2,
                    input_indices_by_name,
                    reach,
                    habitat,
                    taxon,
                    group,
                )
                if ismissing(info) == false
                    proc_infos[proc_name] = info
                end
            end
        end
        if length(proc_infos) > 0
            push!(result[y_name], proc_infos)
        end
    end
    return result
end

function get_proc_web_info(
    y_name_indices::Dict{String,Dict{String,Dict{String,Int}}},
    y_names::Vector{String},
    proc_values_and_indices_taxon1::Dict{String,ValueAndInputIndex{T}},
    kpnames::Dict{String,Vector{String}},
    taxa2::Dict{String,Dict{String,T}},
    parameters::Dict{String,T},
    input_indices_by_name::Dict{String,Int},
    reach,
    habitat,
    taxon,
    group,
)::Maybe{ProcWebInfo{T}} where {T<:Real}
    stoich_infos = Vector{StoichInfo{T}}()
    y_names_indices_reach_hab = y_name_indices[reach][habitat]
    for taxa2_name in keys(taxa2)
        taxa2_stoich = taxa2[taxa2_name]
        state_variables = Vector{StateVariableAndCoeff{T}}()
        for related_taxon in sort(collect(keys(taxa2_stoich)))
            index = y_names_indices_reach_hab[related_taxon]
            coeff = taxa2_stoich[related_taxon]
            push!(state_variables, StateVariableAndCoeff(index, coeff))
        end
        if length(state_variables) > 0
            group2 = group
            taxon2 = taxa2_name
            if haskey(y_names_indices_reach_hab, taxa2_name)
                _, _, _, group2 = split(y_names[y_names_indices_reach_hab[taxa2_name]], "_")
            else
                taxon2 = taxon
            end
            proc_values_and_indices_taxa2 = Dict{String,ValueAndInputIndex{T}}()
            for par_name in kpnames["taxa2"]
                bindings = Dict(
                    "Par" => par_name,
                    "Reach" => reach,
                    "Habitat" => habitat,
                    "Taxon1" => taxon,
                    "Group1" => group,
                    "Taxon2" => taxon2,
                    "Group2" => group2,
                )
                proc_values_and_indices_taxa2[par_name] = get_value_and_index(
                    get_hierarchy_taxa2(),
                    parameters,
                    input_indices_by_name,
                    bindings,
                )
            end
            food_index = get_food_index(state_variables, taxa2_name, y_names)
            info = StoichInfo(
                taxa2_name,
                proc_values_and_indices_taxa2,
                state_variables,
                food_index,
            )
            push!(stoich_infos, info)
        end
    end
    if length(stoich_infos) > 0
        return ProcWebInfo(proc_values_and_indices_taxon1, stoich_infos)
    else
        return missing
    end
end

function get_food_index(
    state_variables::Vector{StateVariableAndCoeff{T}},
    stoich_key::String,
    y_names::Vector{String},
)::Int where {T<:Real}
    food_index = 0
    if stoich_key != "SusPOM"
        stoich_key_part = '_' * stoich_key * '_'
        for state_variable in state_variables
            index = state_variable.state_variable_index
            if occursin(stoich_key_part, y_names[index])
                food_index = index
                break
            end
        end
    end
    return food_index
end

function get_indices_1sthabinreach(
    indfA::Dict{String,Dict{String,Vector{Int}}},
)::Vector{Int}
    return reduce(vcat, [getindex(v["ind_reach"], v["ind_1sthab"]) for v in values(indfA)])
end

function get_hierarchy_taxa2()::Vector{Vector{String}}
    return [
        ["Taxon1", "Taxon2", "Reach", "Habitat", "Par"],
        ["Group1", "Taxon2", "Reach", "Habitat", "Par"],
        ["Taxon1", "Group2", "Reach", "Habitat", "Par"],
        ["Group1", "Group2", "Reach", "Habitat", "Par"],
        ["Taxon1", "Taxon2", "Habitat", "Par"],
        ["Group1", "Taxon2", "Habitat", "Par"],
        ["Taxon1", "Group2", "Habitat", "Par"],
        ["Group1", "Group2", "Habitat", "Par"],
        ["Taxon1", "Taxon2", "Reach", "Par"],
        ["Group1", "Taxon2", "Reach", "Par"],
        ["Taxon1", "Group2", "Reach", "Par"],
        ["Group1", "Group2", "Reach", "Par"],
        ["Taxon1", "Taxon2", "Par"],
        ["Group1", "Taxon2", "Par"],
        ["Taxon1", "Group2", "Par"],
        ["Group1", "Group2", "Par"],
        ["Taxon1", "Reach", "Habitat", "Par"],
        ["Group1", "Reach", "Habitat", "Par"],
        ["Taxon1", "Habitat", "Par"],
        ["Group1", "Habitat", "Par"],
        ["Taxon1", "Reach", "Par"],
        ["Group1", "Reach", "Par"],
        ["Taxon1", "Par"],
        ["Group1", "Par"],
        ["Reach", "Habitat", "Par"],
        ["Habitat", "Par"],
        ["Reach", "Par"],
        ["Par"],
    ]
end

SystemDefinition(model::Model{T}) where {T<:Real} = begin
    y_names = model.y_names
    y_name_indices = get_y_names_indices_by_name_hierarchy(y_names["y_names"])
    parameters = model.parameters
    inputs = model.inputs
    # get global parameters:
    # ----------------------
    par_global = get_values_and_indices_global(
        [
            "kBoltzmann",
            "M0",
            "ftempmax_intercept",
            "ftempmax_curv",
            "fcurrent_intercept",
            "fcurrent_curv",
            "fsapro_intercept",
            "fsapro_curv",
            "forgmicropoll_intercept",
            "forgmicropoll_curv",
            "fmicrohab_intercept",
            "fmicrohab_curv",
        ],
        parameters,
        inputs,
        Dict(
            "kBoltzmann" => 8.61734e-005,
            "M0" => 1.0,
            "ftempmax_intercept" => 0.0,
            "ftempmax_curv" => 0.0,
            "fcurrent_intercept" => 0.0,
            "fcurrent_curv" => 0.0,
            "fsapro_intercept" => 0.0,
            "fsapro_curv" => 0.0,
            "forgmicropoll_intercept" => 0.0,
            "forgmicropoll_curv" => 0.0,
            "fmicrohab_intercept" => 0.0,
            "fmicrohab_curv" => 0.0,
        ),
    )
    par_global_envcondtraits = get_values_and_indices_global_envcondtraits(
        ["tempmaxKval", "currentmsval", "saprowqclassval", "orgmicropollTUval"],
        parameters,
        inputs,
    )
    # get evironmental conditions:
    # ----------------------------
    # TODO: check required "w", "L"
    par_reach = get_values_and_indices_reach(["w", "L"], y_names, parameters, inputs)
    par_habitat = get_values_and_indices_habitat(
        [
            "T",
            "I0",
            "fshade",
            "CP",
            "CN",
            "DSusPOM",
            "tau",
            "taucrit",
            "tempmaxK",
            "currentms",
            "orgmicropollTU",
            "saprowqclass",
            "fA",
            "DFish",
        ],
        y_names,
        parameters,
        inputs,
        Dict("fA" => 1.0, "DFish" => 0.0),
    )
    par_habitat_group = get_values_and_indices_habitat_group(
        ["microhabaf"],
        y_names,
        parameters,
        inputs,
    )
    # get initial conditions and inputs:
    # ----------------------------------
    par_taxon_group_reach_habitat = get_values_and_indices_taxon_group_reach_habitat(
        ["Dini", "Input"],
        y_names["y_names"],
        parameters,
        inputs,
        Dict("Dini" => 0.0, "Input" => 0.0),
    )
    # get taxa properties:
    # --------------------
    par_taxon_group = get_values_and_indices_taxon_group(
        ["M", "Ea", "b", "i0", "EC", "fbasaltax"],
        y_names,
        parameters,
        inputs,
        Dict("b" => 0.75, "fbasaltax" => 1.0),
    )
    # if trait_names are extended, the xval_pars have to be extended as well!

    trait_names = [
        "saprotolval",
        "orgmicropolltolval",
        "currenttolval",
        "tempmaxtolval",
        "microhabtolval",
    ]

    # objects which contain the names of the x-values for interpolation that are used to
    # derive the trait-tolerance values of invertebrates in the correct order

    xval_pars = Dict(
        "saprotolval" => par_global_envcondtraits["saprowqclassval"],
        "orgmicropolltolval" => par_global_envcondtraits["orgmicropollTUval"],
        "currenttolval" => par_global_envcondtraits["currentmsval"],
        "tempmaxtolval" => par_global_envcondtraits["tempmaxKval"],
        "microhabtolval" => par_habitat_group["microhabaf"],
    )
    par_taxaprop_traits =
        get_values_and_indices_taxaprop_traits(xval_pars, y_names, parameters, inputs)
    # get process stoichiometries:
    # ----------------------------
    # taxon-based processes:
    kinparnames_taxon = Dict(
        "Miner" => ["kminer"],
        "Drift" => ["cdet"],
        "Resp" => ["fresp"],
        "Death" => ["fdeath"],
        "Prod" => ["fprod", "fgrotax", "hdens", "KI", "KP", "KN"],
    )
    stoich_taxon = Dict{String,Dict{String,Dict{String,T}}}()
    for proc in keys(kinparnames_taxon)
        stoich_taxon[proc] = get_stoich_taxon(proc, parameters)
    end
    # food web processes:
    # -------------------
    kinparnames_web = Dict(
        "Cons" => Dict(
            "taxon1" => ["fcons", "fgrotax", "hdens", "Kfood", "q"],
            "taxa2" => ["Pref"],
        ),
        "FishPred" => Dict("taxon1" => ["cfish", "Kfood", "q"], "taxa2" => ["Pref"]),
    )
    stoich_web = Dict{String,Dict{String,Dict{String,Dict{String,T}}}}()
    for proc in keys(kinparnames_web)
        stoich_web[proc] = get_stoich_web(proc, parameters)
    end
    # get kinetic parameters of processes:
    # ------------------------------------
    par_proc_taxon = get_par_proc_taxon(
        kinparnames_taxon,
        stoich_taxon,
        y_name_indices,
        y_names["y_names"],
        parameters,
        inputs,
    )
    par_proc_web = get_par_proc_web(
        kinparnames_web,
        stoich_web,
        y_name_indices,
        y_names["y_names"],
        y_names["indfA"],
        parameters,
        inputs,
    )
    par_proc_web_cons = get_web_info(par_proc_web, "Cons")
    par_proc_web_fishpred = get_web_info(par_proc_web, "FishPred")
    max_stoich_infos_length = max(
        get_max_stoich_infos_length(par_proc_web_cons),
        get_max_stoich_infos_length(par_proc_web_fishpred),
    )
    par_env = merge(par_reach, par_habitat)
    par_trait_cond = get_par_trait_cond(
        par_taxaprop_traits,
        par_env,
        par_habitat_group,
        par_global_envcondtraits,
        par_global,
    )
    y_names_names = y_names["y_names"]
    SystemDefinition(
        y_names,
        parameters,
        inputs,
        par_global,
        par_env,
        par_habitat_group,
        par_taxon_group_reach_habitat,
        par_taxon_group,
        stoich_taxon,
        change_dict_to_arry_via_y_names(par_proc_taxon, y_names_names),
        change_dict_to_arry_via_y_names(par_proc_web_cons, y_names_names),
        change_dict_to_arry_via_y_names(par_proc_web_fishpred, y_names_names),
        max_stoich_infos_length,
        par_trait_cond,
    )
end

function change_dict_to_arry_via_y_names(
    dict::Dict{String,T},
    y_names::Vector{String},
)::Vector{T} where {T<:Any}
    result = T[]
    for i = 1:length(y_names)
        push!(result, dict[y_names[i]])
    end
    return result
end

function get_max_stoich_infos_length(
    web::Dict{String,Maybe{ProcWebInfo{T}}},
)::Int where {T<:Real}
    result = 0
    for e in web
        if e.second !== missing
            result = max(result, length(e.second.stoich_infos))
        end
    end
    return result
end

function get_web_info(
    web::Dict{String,Vector{Dict{String,ProcWebInfo{T}}}},
    name::String,
)::Dict{String,Maybe{ProcWebInfo{T}}} where {T<:Real}
    result = Dict{String,Maybe{ProcWebInfo{T}}}()
    for e in web
        result[e.first] = may_get_web(e.second, name)
    end
    return result
end

function may_get_web(
    web::Vector{Dict{String,ProcWebInfo{T}}},
    name::String,
)::Maybe{ProcWebInfo{T}} where {T<:Real}
    for e in web
        if haskey(e, name)
            return e[name]
        end
    end
    return missing
end

function get_par_trait_cond(
    par_taxaprop_traits::Dict{String,Dict{String,ValuesAndInputIndices{T}}},
    par_env::Dict{String,ValuesAndInputIndices{T}},
    par_habitat_group::Dict{String,Dict{String,ValuesAndInputIndices{T}}},
    par_global_envcondtraits::Dict{String,Dict{String,ValueAndInputIndex{T}}},
    par_global::Dict{String,ValueAndInputIndex{T}},
)::TraitCondition{T} where {T<:Real}
    n = length(par_env["fA"].values)
    result = Dict{String,Vector{T}}()
    # saprobic conditions:
    # --------------------
    fsapro = calculate_trait_cond(
        par_taxaprop_traits,
        par_env,
        par_global_envcondtraits,
        par_global,
        n,
        "fsapro",
        "saprowqclass",
        "saprotolval",
    )
    # organic toxicants:
    # ------------------
    forgmicropoll = calculate_trait_cond(
        par_taxaprop_traits,
        par_env,
        par_global_envcondtraits,
        par_global,
        n,
        "forgmicropoll",
        "orgmicropollTU",
        "orgmicropolltolval",
    )
    # current preference:
    # -------------------
    fcurrent = calculate_trait_cond(
        par_taxaprop_traits,
        par_env,
        par_global_envcondtraits,
        par_global,
        n,
        "fcurrent",
        "currentms",
        "currenttolval",
    )
    # temperature tolerance:
    # ----------------------
    ftempmax = calculate_trait_cond(
        par_taxaprop_traits,
        par_env,
        par_global_envcondtraits,
        par_global,
        n,
        "ftempmax",
        "tempmaxK",
        "tempmaxtolval",
    )
    # microhabitat:
    # -------------
    fmicrohab =
        calculate_group_cond(par_taxaprop_traits, par_env, par_habitat_group, par_global, n)
    return TraitCondition(fcurrent, fmicrohab, forgmicropoll, fsapro, ftempmax)
end

function calculate_trait_cond(
    par_taxaprop_traits::Dict{String,Dict{String,ValuesAndInputIndices{T}}},
    par_env::Dict{String,ValuesAndInputIndices{T}},
    par_global_envcondtraits::Dict{String,Dict{String,ValueAndInputIndex{T}}},
    par_global::Dict{String,ValueAndInputIndex{T}},
    n::Int,
    trait_cond_name::String,
    trait_class_name::String,
    trait_class_map_name::String,
)::Vector{T} where {T<:Real}
    trait_class_val_name = trait_class_name * "val"
    result = ones(T, n)
    default_global = ValueAndInputIndex(zero(T), 0)
    if haskey(par_global_envcondtraits, trait_class_val_name)
        class = par_global_envcondtraits[trait_class_val_name]
        if length(class) > 0
            map_value = par_taxaprop_traits[trait_class_map_name]
            values =
                get(par_env, trait_class_name, create_dummy_values_and_input_indices(n))
            sorted_keys = sort(collect(keys(class)))
            x = zeros(T, length(sorted_keys))
            y = [zeros(T, length(sorted_keys)) for i = 1:n]
            for i = 1:length(sorted_keys)
                key = sorted_keys[i]
                x[i] = class[key].value
                v = get(map_value, key, create_dummy_values_and_input_indices(n))
                for j = 1:length(v.values)
                    y[j][i] = v.values[j]
                end
            end
            intercept =
                get(par_global, trait_cond_name * "_intercept", default_global).value
            curv = get(par_global, trait_cond_name * "_curv", default_global).value
            for i = 1:n
                v = interpolate(x, y[i], values.values[i])
                if isnan(v) == false
                    result[i] = exp_transform(v, intercept, curv)
                end
            end
        end
    end
    return result
end

function calculate_group_cond(
    par_taxaprop_traits::Dict{String,Dict{String,ValuesAndInputIndices{T}}},
    par_env::Dict{String,ValuesAndInputIndices{T}},
    par_habitat_group::Dict{String,Dict{String,ValuesAndInputIndices{T}}},
    par_global::Dict{String,ValueAndInputIndex{T}},
    n::Int,
)::Vector{T} where {T<:Real}
    result = ones(T, n)
    if haskey(par_habitat_group, "microhabaf")
        group = par_habitat_group["microhabaf"]
        if length(group) > 0
            default_global = ValueAndInputIndex(zero(T), 0)
            intercept = get(par_global, "fmicrohab_intercept", default_global).value
            curv = get(par_global, "fmicrohab_curv", default_global).value
            sorted_keys = sort(collect(keys(group)))
            tolval = par_taxaprop_traits["microhabtolval"]
            for i = 1:n
                sum = zero(T)
                for key in sorted_keys
                    x = group[key].values[i]
                    y = tolval[key].values[i]
                    if isnan(x) || isnan(y)
                        sum = one(T)
                        break
                    end
                    sum += x * exp_transform(y, intercept, curv)
                end
                result[i] = sum
            end
        end
    end
    return result
end

function exp_transform(x::T, intercept::T, curv::T)::T where {T<:Real}
    intercept - (intercept - 1) * expt(x, curv)
end

function expt(x::T, curv::T)::T where {T<:Real}
    if curv == zero(T)
        if isa(x, ForwardDiff.Dual)
            # Handle removable singularity at curv = 0
            tag = typeof(x).parameters[1]
            xx = ForwardDiff.value(x)
            p = ForwardDiff.partials(x) + 0.5 * xx * (1 - xx) * ForwardDiff.partials(curv)
            ForwardDiff.Dual{tag}(xx, p)
        else
            x
        end
    else
        (1 - exp(-curv * x)) / (1 - exp(-curv))
    end
end

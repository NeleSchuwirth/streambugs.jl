module Streambugs

export run_streambugs # Streambugs.jl
export Input # streambugs_types.jl
export construct_statevariables, decode_statevarnames  # streambugs_aux.jl
export Model, load_model_parameters, get_parameter_names_values, get_y_ini # streambugs_model.jl
export streambugs_example_model_toy, streambugs_example_model_extended # streambugs_examples.jl

import OrdinaryDiffEq
import ForwardDiff

include("streambugs_types.jl")
include("utilities.jl")
include("streambugs_aux.jl")
include("streambugs_model.jl")
include("streambugs_examples.jl")
include("streambugs_rhs.jl")

function _run_streambugs(
    sys_def::SystemDefinition,
    y_ini::Vector{<:Real},
    times::Vector{<:Real};
    algorithm = OrdinaryDiffEq.Tsit5(),
    reltol = 1e-4,
    abstol = 1e-4,
)
    problem = streambugs_problem(y_ini, times, sys_def)
    return OrdinaryDiffEq.solve(
        problem,
        algorithm,
        reltol = reltol,
        abstol = abstol;
        saveat = times,
    )
end

"""
    run_streambugs(model, times; y_ini, parameter_names, parameter_values, algorithm, dt, reltol, abstol)

Run the streambugs ODE model for `model::Model` for the time points `times::{<:Real}`.
It returns an `ODESolution` object.

Optinal keyword parameters are
- `y_ini`: initial state. Default: `Streambugs.get_y_ini(model)`
- `parameter_names`: Subset of parameters of the model (Default: empty vector).
- `parameter_vales`: Values of the parameters specified by `parameter_names` (Default: empty vector). These values are replacing the values specified in the `model`.
- `algorithm`: ODE integration algorithm (Default: `OrdinaryDiffEq.Tsit5()`).
- `dt`: Fixed time step to be used by the ODE integrator (Default: `nothing`). If not specified the integrator controls the step size.
- `reltol`: Relative tolerance of the integration error (Default: `1e-4`).
- `abstol`: Absolute tolerance of the integration error (Default: `1e-4`).

# Example
```julia-repl
julia> result = run_streambugs(streambugs_example_model_toy(), Vector(0:200)/100)
julia> result.t[10]
0.09
julia> result.u[10]
54-element Vector{Float64}:
 754.2336080932157
  41.32458756857696
 154.3805595354235
 178.22677866772605
  16.182308988434034
  18.68986028199015
   ⋮
  31.84240572735045
  36.59176423916551
  36.59176423916551
  36.59176423916551
  31.84240572735045
```
"""
function run_streambugs(
    model::Model,
    times::Vector{<:Real};
    y_ini::Vector{<:Real} = Streambugs.get_y_ini(model),
    parameter_names::Vector{String} = Vector{String}(),
    parameter_values::Vector{T} = Vector{Float64}(),
    algorithm = OrdinaryDiffEq.Tsit5(),
    dt = nothing,
    reltol = 1e-4,
    abstol = 1e-4,
) where {T<:Real}
    number_of_parameter_names = length(parameter_names)
    number_of_parameters = length(parameter_values)
    if number_of_parameter_names != number_of_parameters
        throw(
            ArgumentError(
                "Number of parameter names ($(number_of_parameter_names)) doesn't match " *
                "number of parameter values ($(number_of_parameters)).",
            ),
        )
    end
    if number_of_parameter_names == 0
        return _run_streambugs(
            Streambugs.SystemDefinition(model),
            y_ini,
            times;
            algorithm,
            reltol,
            abstol,
        )
    end
    cache = Dict()
    function rhs_wrapper(dydt, y_original, pars_rhs, time)
        if haskey(cache, pars_rhs)
            sys_def = cache[pars_rhs]
        else
            npar = length(parameter_names)
            parameters = merge(
                model.parameters,
                Dict(parameter_names[i] => pars_rhs[i] for i = 1:npar),
            )
            float_type = npar > 0 ? typeof(pars_rhs[1]) : Float64
            inputs = Vector{Input{float_type}}()
            for input in model.inputs
                name = input.name
                if isa(input.value, Matrix)
                    value = convert(Matrix{float_type}, input.value)
                else
                    value = convert(float_type, input.value)
                end
                push!(inputs, Input(name, value))
            end
            sys_def = Streambugs.SystemDefinition(
                Model(model.name, model.y_names, parameters, inputs),
            )
            cache[pars_rhs] = sys_def
        end
        return streambugs_rhs(dydt, y_original, sys_def, time)
    end
    problem = streambugs_problem(y_ini, times, parameter_values, f = rhs_wrapper)
    extra_kwargs = ()
    if dt !== nothing
        extra_kwargs = (dt = dt, adaptive = false)
    end
    OrdinaryDiffEq.solve(
        problem,
        algorithm;
        saveat = times,
        reltol = reltol,
        abstol = abstol,
        pairs(extra_kwargs)...,
    )
end

function streambugs_problem(
    y_ini::Vector{<:Real},
    times::Vector{<:Real},
    p;
    f::Function = streambugs_rhs,
)
    return OrdinaryDiffEq.ODEProblem(f, y_ini, (times[1], times[end]), p)
end

end  # module

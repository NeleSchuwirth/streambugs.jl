# Streambugs

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://NeleSchuwirth.gitlab.io/Streambugs.jl/dev)
[![Build Status](https://gitlab.com/NeleSchuwirth/Streambugs.jl/badges/master/pipeline.svg)](https://gitlab.com/NeleSchuwirth/Streambugs.jl/pipelines)
[![Coverage](https://gitlab.com/NeleSchuwirth/Streambugs.jl/badges/master/coverage.svg)](https://gitlab.com/NeleSchuwirth/Streambugs.jl/commits/master)

Parametric ordinary differential equations model of growth, death, and respiration of
macroinvertebrate and algae taxa; cf.
https://www.eawag.ch/en/department/siam/projects/streambugs/ .

This is a Julia version of the
[streambugs R package](https://cran.r-project.org/web/packages/streambugs/index.html).

## Installation

Start Julia REPL and install `Streambugs.jl` by running the following commands:
```julia-repl
] add https://gitlab.com/NeleSchuwirth/streambugs.jl
```

## Examples

#### Run a simulation
Run a toy model simulations in Julia REPL:
```julia
using Streambugs
names(Streambugs)
model = streambugs_example_model_toy()
result = run_streambugs(model, Vector(0:200)/100)
result.t  # time vector
result.u[4]  # result vector (y) at 4-th time point (t=0.03)
result(0.03)  # result vector (y) at time 0.03
model.y_names["y_names"]  # names of the elements of the result vector
```

#### Compute a gradient
Compute a [ForwardDiff.jl](https://github.com/JuliaDiff/ForwardDiff.jl) gradient of
a sum squared error with respect to observed values:
```julia
using Streambugs
model = streambugs_example_model_toy()
# state variables
y_names = model.y_names["y_names"]
# collect all fresp parameters for differentiation (one per taxon)
par_names, par_values = get_parameter_names_values(
    model;
    name_filter=(pn) -> endswith(pn, r"_fresp"),
)
# observed variable index
y_index = 5
# observed variable name
y_names[y_index]
# observation time points
times = [0.00, 0.25, 0.50, 0.75, 1.00]
# observation values
observed = [3.8, 0.45, 0.25, 0.22, 0.21]

# loss function: mean squared error
mse(x, y) =  mean(abs2, x .- y)

solution = run_streambugs(model, times)
mse(solution[y_index, :], observed)

# ForwardDiff.gradient of mse wrt par_names at par_values
using ForwardDiff

function loss(p)
    sol = run_streambugs(model,
                         times;
                         parameter_values = p,
                         parameter_names = par_names,
    )
    mse(sol[y_index, :], observed)
end
# gradient of mse wrt par_names at par_values
@time dpar_values = ForwardDiff.gradient(loss, par_values)
```

## Run streambugs from R

With the help of JuliaConnectoR (see https://cran.r-project.org/package=JuliaConnectoR) it is possible to call the streambugs.jl from R.

Prerequisites:
* Julia 1.7 is installed.
* Git repository `streambugs.jl` is cloned.
* JuliaConnectoR is installed (by running `install.packages("JuliaConnectoR")` in R console).

### Example

Start R or RStudio and run the following commands inside the console:

```julia
library(JuliaConnectoR)
juliaEval('import Pkg;Pkg.activate("path/to/streambugs.jl")')   # Evaluate a Julia statement
juliaEval('Pkg.instantiate()')
juliaEval('using Streambugs')
model <- juliaEval('streambugs_example_model_toy()')    # Return the result as a JuliaProxy object
run_streambugs <- juliaFun("run_streambugs")    # Defines an R proxy of a Julia function
result.julia <- run_streambugs(model, (0:200)/100, reltol=1e-5) # Run the R proxy using normal R arguments
result <- juliaGet(result.julia)    # Turn the wrapped Julia result into an R object
result$t
result$u[4]

# Get filtered parameters
pp <- juliaGet(juliaLet('get_parameter_names_values(model;
                         name_filter=n -> endswith(n, r"_fresp"))',
                        model = model))
par.names <- pp[[1]]
par.values <- pp[[2]]
times <- c(0.00, 0.25, 0.50, 0.75, 1.00)
observed <- c(3.8, 0.45, 0.25, 0.22, 0.21)
# Define mean squared error cost function mse in Julia
juliaEval('mse(x,y;rel_err=0.05) = sum((x .- y) .^ 2 ./ (rel_err .* abs.(y)))')
# Define run_func in Julia with provided parameters
juliaLet('global run_func(v) = run_streambugs(model,times;parameter_names=pnames,parameter_values=v)',
         model=model, times=times, pnames=par.names)
# Import and use package ForwardDiff
juliaEval('import Pkg; Pkg.add("ForwardDiff"); using ForwardDiff')
# Calculate the gradient of the cost function
grad <- juliaLet('ForwardDiff.gradient(p -> mse(run_func(p)[5,:],observed),values)',
                 observed=observed, values=par.values)
```

## Development

Clone `Streambugs.jl` repository at https://gitlab.com/NeleSchuwirth/streambugs.jl.git, start
Julia REPL in the directory of the cloned repository.

First time, add globally development dependencies, setup local environment and install
in it package dependencies:
```julia-repl
julia>]
pkg> add Revise
pkg> add JuliaFormatter
pkg> activate .
pkg> instantiate
```

In subsequent runs, you need to run `pkg> instantiate`  only on upstream change of
dependencies; otherwise run:
```julia-repl
julia>]
pkg> activate .
pkg> <ctrl+c>
julia> using Revise
julia> import Streambugs
```

Before each push or merge remember to:

1. Test locally:

    ```julia-repl
    julia>]
    pkg> activate .
    pkg> test
    ```

2. Format the code:

    ```julia-repl
    julia> import JuliaFormatter
    julia> JuliaFormatter.format(".")
    ```



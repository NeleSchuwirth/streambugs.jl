using Streambugs
using Documenter

makedocs(;
    modules = [Streambugs],
    authors = "Mikolaj Rybinski <mikolaj.rybinski@id.ethz.ch> and contributors",
    repo = "https://gitlab.com/mikolajr/Streambugs.jl/blob/{commit}{path}#L{line}",
    sitename = "Streambugs.jl",
    format = Documenter.HTML(;
        prettyurls = get(ENV, "CI", "false") == "true",
        canonical = "https://mikolajr.gitlab.io/Streambugs.jl",
        assets = String[],
    ),
    pages = ["Home" => "index.md"],
)
